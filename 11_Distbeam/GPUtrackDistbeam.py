#SVF 
#Start 09.09.2016 - GPU 16.11.2016
# Perform tracking of distribution

import os, sys
import numpy as np
import time
import datetime as dt
import matplotlib
import scipy.constants as const
import scipy.special as spec
import pickle as pkl

prevdevice=0
if os.path.isfile("cudaDevice.txt"):
    prevdevice = np.loadtxt('cudaDevice.txt')
    thisdevice = '1' if int(prevdevice)==3 else '3'#str(int(prevdevice)+2)
#os.environ['CUDA_DEVICE'] = thisdevice    # str(int((prevdevice+1)%4))
if int(sys.argv[1][0]) > 0:
    os.environ['CUDA_DEVICE'] = str(int(sys.argv[1][0])%4)
else:
    os.environ['CUDA_DEVICE'] = '3'
thisdevice = os.environ['CUDA_DEVICE']

import pycuda.driver as cuda
import pycuda.compiler as comp
import pycuda.gpuarray as gp
import pycuda.autoinit
from pycuda.autoinit import context

import moduleSondre as mods

import tuneDistbeam as modTune 
from jacobsen import *


# # Work in reduced units (sigma, beta, alpha = twiss parameters)
# # x  = x/sigma
# # xp = (alpha*x + beta*x')/sigma , xprime 
# # then -> use regular rotation matrix for betatron oscillations

# ########################
# Input from terminal
# python3 distbeam.py IPS Qx0 Qy0 XI maxA N iT mT p nQ Dist_Y_ dQ Qp

#############################################################################
#############################################################################
def weakstrong():
    print('\ntrackDistbeam.py   - '+str(dt.datetime.now().time())[:8])
 
    if np.size(sys.argv)<14:
        print("Not enough arguments given \nIPS Qx0 Qy0 XI maxA N iT mT p nQ Dist_Y_P dQ Qp")
        sys.exit(1)
        
    filename = mods.makeFilename(sys.argv)
    print(filename)   
    
    # Where to save files
    savedir = '02_pickle_save'
    if not os.path.isdir(savedir):
        os.makedirs(savedir)
    savedir += "/"+filename        #: Where to save pickle files
    if mods.doit(filename):
        os.system('rm -rf '+ savedir)
    else:
        print('Path already exists, tracking exists\n'+savedir)
        sys.exit(4)

    os.makedirs(savedir)
    time.sleep(0.1)
    if os.environ['PYTHONPATH'].find('sondrevf')<0:
        np.savetxt('cudaDevice.txt',[int(thisdevice)])
    ######################################################
    ################# Physical constants #################
    PI = const.pi
    mP = const.m_p                              #: = 1.672621637e-27
    c = const.c                                 #: = 299792458
    e = const.e                                 #: = 1.602176487e-19
    e0 = const.physical_constants['electric constant'][0]   # = 8.854...
    rp = e**2/(4*PI*e0*mP*c**2)                 # Definition of rp - classical radius of proton
    
    ######################################################
    ################ Physical parameters #################
    Z = 1                                      # : Protons
    # Take in parameters from args
    IPS = sys.argv[1]                           #: What happens in one lap. [HO vert HO hor Noise Chrom]
    IPS_np=np.int32([int(IPS[i]) for i in range(len(IPS))])
    IP2 = (IPS_np[0]+IPS_np[1])*(IPS_np[2]+IPS_np[3])#: True if 2 IPs in simulation
    Qx0 = np.float64(sys.argv[2])                    #: Machine tune for total oscillation - horizontal plane
    Qy0 = np.float64(sys.argv[3])                    #: Machine tune for total oscillation - vertical plane
    dQ  = np.float64(sys.argv[12]) if IP2 else 0     #: Machine tune between IP1 and IP5   - horizontal plane
    Qy1 = Qx1 = dQ                                   #: Machine tune between IP1 and IP5   - vertical plane
    if dQ == 0.125 : Qy1 = np.float64(1./12)        #: Super cancelling, 4,3,resonance , better=0.25
    if dQ >= 1 : Qx1 = Qx0/2+dQ-1.5  ; Qy1 = Qy0/2 + dQ-1.5
    Qx2 = Qx0-Qx1 ; Qy2 = Qy0-Qy1                    #: Machine tunes between IP5 and IP1 
    XI = -Z*np.float64(sys.argv[4])                     #: Be-Be-parameter
    emitMACH = 3.5e-6
    
    # Beam at IP
    gamma = 6500.0/0.938272                     #: 6.5 TeV / restmass of proton
    betaIP = eval(sys.argv[16])                 # beta at interaction points
    crossing2 = IPS_np[2]-1
    
    # Strong beam  - Assumed round for now
    nSliceGiven  = int(eval(sys.argv[15]))      #: # slices of strong beam - 21 is standard
    crossAS = eval(sys.argv[14])*1e-6           #: microradians crossing angle
    betaIPx = eval(sys.argv[16])
    betaIPy = eval(sys.argv[17])
    emitNXS= np.float64(sys.argv[21])           #: Normalized horizontal emittance of strong beam
    emitNYS= np.float64(sys.argv[22])           #: Normalized vertical   emittance of strong beam
    emitXS = emitNXS/gamma
    emitYS = emitNYS/gamma
    sigXS = np.sqrt(emitXS*betaIPx)
    sigPS = np.sqrt(emitXS/betaIPx)
    sigYS = np.sqrt(emitYS*betaIPy)
    sigQS = np.sqrt(emitYS/betaIPy)
    sigSS = 0.08                                #: real longitudinal spacial amplitude
    sigDS = 1e-4                                #: real longitudinal momentum deviation amplitude
    XI_Y = XI
    intensity = -XI_Y*Z*gamma*2*PI*sigYS*(sigYS+sigXS)/(betaIPy*rp) #: Number of particles per strong bunch (not per slice)
    if intensity == 0: intensity = 1e11
    
    # Weak beam
    maxA = float(sys.argv[5])                   #: Max amplitude if choose uniform distribution
    nPart  = int(eval(sys.argv[6]))             #: # of particles in weak beam
    crossAW = crossAS
    emitNXW= np.float64(sys.argv[19])           #: Normalized horizontal emittance of weak beam
    emitNYW= np.float64(sys.argv[20])           #: Normalized vertical   emittance of weak beam
    emitXW = emitNXW/gamma
    emitYW = emitNYW/gamma
    sigXW = np.sqrt(emitXW*betaIPx)
    sigPW = np.sqrt(emitXW/betaIPx)
    sigYW = np.sqrt(emitYW*betaIPy)
    sigQW = np.sqrt(emitYW/betaIPy)
    sigSW = sigSS
    sigDW = sigDS
    
    # separation of beams at IP  
    sepIPn = eval(sys.argv[18])                 # separation at IP transverse to crossing angle (in sigmas of strong beam)
    sepX = sepIPn*sigXS  ; sepY = sepIPn*sigYS  
    # Beambeam Prefactors / "Constants"
    headonC = -Z*8*PI*XI                           #: Coefficient of Headon interaction (should be negative...)
    nLR = 32                                    #: number of long range interactions per IP
    longrangeC = headonC*nLR                    #: Coefficient of long range interaction
    normsepLR = crossAS*np.sqrt(gamma*betaIP/emitNXS)
    
    # Noise    
    noiseA = 0 if (not IPS_np[4]) else ([1e-4,10**(-2-IPS_np[4]/2)][1*IPS_np[4]!=1])

    # 6D chromaticity
    Qs = [0,2e-3,1./12500,1./25000][IPS_np[5]]               #: Synchrotron frequency LHC = 0.00212  , FCC = 0.0019
    Qs1 = Qs*0.5*(IP2>0) ; Qs2 = Qs*0.5*(2-1*(IP2>0))
    Qp = np.float64(eval(sys.argv[13]))*(IPS_np[5]!=0)
    Qp1 = Qp*0.5 if IP2 else 0                  #: Chromaticity of first arc
    Qp2 = Qp*0.5 if IP2 else Qp                 #: Chromaticity of second arc

    CH = (Qp1!=0 or Qp2!=0)
    LAT1 = (Qp1!=0 or Qx1!=0 or Qy1!=0 or IP2 or Qs1!=0)*1
    LAT2 = (Qp2!=0 or Qx2!=0 or Qy2!=0 or IP2 or Qs2!=0)*1
    
    
    ########################################################
    ################# Simulation parameters ################
    # Take in parameters from args
    inTurn = int(float(sys.argv[7])*10**3)      #: # of turns before first good measurement
    midTurn = int(float(sys.argv[8])*10**3)     #: # of turns between measurements
    ln2turn = int(sys.argv[9])                  #: "Precision" in tune measurement - # number of turns
    nSave   = int(1024*2**int(round(ln2turn)))  #: # of turns = 1024*2**N, to save for tunes
    nQ = int(sys.argv[10])                      #: # of tunes / emittance measurements to make
    Dist = sys.argv[11]                         #: Which initial distribution to apply
    nMean = 2048                                #: Number of turns to average J over
    dynAp = maxA if (maxA>=4 and maxA<6) else 6
    dynAp = 5
    machAp = 5.5 # really 6
    lost_beamx = emitNXW/(dynAp**2*emitNXS)
    lost_beamy = emitNYW/(dynAp**2*emitNYS)
    lost_machx = emitNXW/(machAp**2*emitMACH)
    lost_machy = emitNYW/(machAp**2*emitMACH)
    
    lost_beam = (max(sigXS,sigYS) * dynAp)**2       #: Limit for when particles are "lost"
    lost_mach = (np.sqrt(emitMACH*max(betaIPx,betaIPy)/gamma)*machAp)**2  #: 5.5 or 6 sigma
    
    # Program decisions
    bool_savePlotPkl = True                     #: if want to save plots and pickle files
    bool_stopstdout = 1                         #: IF Want to suppress output from different things
    bool_tune = nSave > 1000                    #: IF want to save turns to calculate tunes
    threads = int(min(512,nPart))               #: How many threads per block 
    
    # How to divide up number of particles - Memory: x,xp,y,yp,s,d, inbeam   - xMat,yMat,xpMat,ypMat
    nSave = nSave if (nSave > 1000) else 1      #: How many turns to save
    maxBatch= int(1e5) if (nSave<1000) else int(0.5*10240*2**(3-ln2turn))        #: 1e5 or less per run
    indBatch = np.append(np.arange(0,nPart,maxBatch),nPart).astype(int)    #: Divide up number of particles in batches
    nBatch = indBatch.size-1                    #: # of batches
    
    # How to divide up number of turns
    turnArr = np.zeros(2+nQ)                    #: Array to keep number of turns to track at a time
    midTurn = np.max([midTurn,nSave,nMean])     #: Turns to do between measurements          
    if nQ>0:  turnArr[2:] = midTurn
    bool_tuneArr = (turnArr==midTurn)*bool_tune #: What turn-set shall be used to calculate tunes
    turnArr[1] = inTurn                         #: Must be after "bool_tuneArr"-line, to ensure right behaviour
    if nQ>0:
        if not bool_tune:
            turnArr[2] = max(midTurn-inTurn,0)
        turnArr[-1] = max(nSave, nMean)             #: Must be after previous line, to ensure right behaviour
    sizeTurnArr = np.size(turnArr)
    
    #################################################################
    ################# Find number of slices #########################
    nSlice = nSliceGiven
    testSet = int(1e3)
    onesies = np.linspace(-10,10,testSet).astype(np.float64)
    zeroes  = np.zeros_like(onesies).astype(np.float64)
    oneg = gp.to_gpu(onesies) ; nulg = gp.to_gpu(zeroes)
    errTooBig = True
    errs = np.zeros(5)
    sliceInc = 0    ;   nsTol = 0.002
#    errs = [] # errs += [max(errTrans,errLong)]
    filestring = ''
    while sliceInc <= 80-nSliceGiven   and errTooBig and nSliceGiven>0:
        if sliceInc ==0:
            nSlice = 200
        else:
            nSlice = nSliceGiven-1+sliceInc

        # Make slices
        sliceX,sliceY,sliceS,  sigXXS_HO, sigYYS_HO, sigPPS_HO, sigQQS_HO = mods.STSLD(
            sigSS = sigSS,sigDS=sigDS,betaXS=betaIPx,emitXS=emitXS,
            betaYS=betaIPy,emitYS=emitYS,nSlice=nSlice,crossAS=crossAS)
        ZNR_G = -XI_Y*2*np.pi*sigYS*(sigYS+sigXS)/(betaIPy*nSlice)

        # Initiate test particles
        if sliceInc==0:
            x,xp,y,yp,s,d = mods.testIC(3,1000,10)
        xgT = gp.to_gpu(x)   ;        xpgT= gp.to_gpu(xp)
        ygT = gp.to_gpu(y)   ;        ypgT= gp.to_gpu(yp)
        sgT = gp.to_gpu(s)   ;        dgT = gp.to_gpu(d)
        testset=x.size
        
        # Initiate function
        modGPUtestNSLICE = comp.SourceModule(KERNEL_TEST.format(
                    SQRPI2 = 2*np.sqrt(const.pi) ,
                    sliceX=mods.cstr(sliceX), sliceS =mods.cstr(sliceS),
                    NSLICE=nSlice,
                    nPart = testSet,
                    ZNR_G = ZNR_G,
                    SAW = np.sin(crossAW), CAW = np.cos(crossAW),TAW = np.tan(crossAW),
                    SEPX = sepX , SEPY = sepY , SEPN = sepIPn,
                    SIGXW=sigXW, SIGPW=sigPW, SIGYW=sigYW, SIGQW=sigQW, SIGSW=sigSW,SIGDW=sigDW, #set initially
                    SIGXXS=sigXXS_HO, SIGYYS=sigYYS_HO, SIGPPS=sigPPS_HO, SIGQQS=sigQQS_HO,    # set by STSLD
                    )
        , no_extern_c = True)
        trackGPUtestNSLICE = modGPUtestNSLICE.get_function('trackGPUtestNSLICE')
        trackGPUtestNSLICE(xgT,xpgT,ygT,ypgT,sgT,dgT,grid = (int(testSet/500+0.5),1,1),block = (int(500),1,1))
        
        # Calculate errors
        DBM = np.array([xgT.get()-x,xpgT.get()-xp,ygT.get()-y,ypgT.get()-yp,dgT.get()-d])
        if sliceInc==0:
            DBM200 = DBM
        if sliceInc > 0:
            for i in range(5):
                rel = np.max(np.abs(DBM200[i]))
                if rel ==0:
                    errs[i]=0
                else:
                    errs[i] = np.sqrt(np.mean((DBM[i]-DBM200[i])**2))/np.max(np.abs(DBM200[i]))
            maxerr= np.max(errs[np.isfinite(errs)])
            string= (('NS = %d | E = %.5f | e(x) = %.2e | e(xp) = %.2e |'+
                                        ' e(y) = %.2e | e(yp) = %.2e | e(d) = %.2e ')%(
                                    nSlice,maxerr,errs[0],errs[1],errs[2],errs[3],errs[4]))
            print(string)
            filestring+=string
            errTooBig = maxerr>nsTol
        sliceInc += 1
#        xgT = oneg.copy()
#        sgT = nulg.copy()
#        xpgT = nulg.copy() ; ygT = nulg.copy() ; ypgT = nulg.copy() ;  dgT = nulg.copy()
#        trackGPUtestNSLICE(xgT,xpgT,ygT,ypgT,sgT,dgT,grid = (int(testSet/200+0.5),1,1),block = (int(200),1,1))
#        if sliceInc==0:
#            arrTrans = xpgT.get()
#        if sliceInc > 0:
#            errTrans = np.sqrt(np.mean(((xpgT.get()-arrTrans)/np.max(arrTrans))**2))

#        # Second, test longitudinal kick
#        xgT = nulg.copy()
#        sgT = oneg.copy()
#        xpgT = nulg.copy() ; ygT = nulg.copy() ; ypgT = nulg.copy() ;  dgT = nulg.copy()
#        trackGPUtestNSLICE(xgT,xpgT,ygT,ypgT,sgT,dgT,grid = (int(testSet/200+0.5),1,1),block = (int(200),1,1))
#        if sliceInc==0:
#            arrLong = dgT.get()
#        if sliceInc > 0:
#            errLong = np.sqrt(np.mean(((dgT.get()-arrLong)/np.max(arrLong))**2))
#            filestring+= '# slices = %d | Trans Rel Err = %.5f | Long Rel Err = %.5f\n'%(nSlice,errTrans,errLong)
#            errTooBig = (errTrans>nsTol) or (errLong>nsTol)
            
        
    if nSliceGiven>0: del(oneg,nulg,xgT,xpgT,ygT,ypgT,dgT,sgT)
    with open(savedir+'/NSLICE_%s.txt'%nSlice,'w') as nslicefile:
        nslicefile.write(filestring)
    
    ZNR_G = -XI_Y*2*PI*sigYS*(sigYS+sigXS)/(betaIPy*max(nSlice,1))
    # How to divide up the slices
    sliceX,sliceY,sliceS,sigXXS_HO, sigYYS_HO, sigPPS_HO, sigQQS_HO = mods.STSLD(
        sigSS = sigSS,sigDS=sigDS,betaXS=betaIPx,emitXS=emitXS,
        betaYS=betaIPy,emitYS=emitYS,nSlice=nSlice,crossAS=crossAS)
 
    ##########################################################
    #################### Output to user ###################### 
    print("%-25s%d"%('# of turns for tunes = ', nSave))
    print("%-25s%d"%('# initial turns = ', inTurn))
    print("%-25s%d"%('# intermediate turns = ', midTurn))
    print("%20s%d"%('N particles = ', nPart))
    print("%20s%.4f"%('Qx1 = ',Qx1))
    print("%20s%.4f"%('Qx2 = ',Qx2))
    print("%20s%.4f"%('Qy1 = ',Qy1))
    print("%20s%.4f"%('Qy2 = ',Qy2))
    print("%20s%d"%('maxBatch  = ' , maxBatch))
    print("%20s%d"%('# batches = ' , nBatch))
    print("%20s%d"%('# threads = ', threads))
    print("%20s%.2f"%('normsepLR = ',normsepLR))
    print("%20s%.3f"%('XI = ',XI))
    print("%20s%.2e, %.2e"%('emittNormStrong = ',emitNXS, emitNYS))
    print("%20s%.2e, %.2e"%('emittNormWeak = ',emitNXW, emitNYW))
    print("%20s%.2e"%('Strong Intensity = ',intensity))
    print("%20s%.2e"%('Noise = ',noiseA))
    print("%20s%.f (%.2f,%.2f)"%('Chromaticity = ',Qp,Qp1,Qp2))
    print("%20s%.0e (%.0e,%.0e)"%('Synchro Tune Qs = ',Qs,Qs1,Qs2))
    print("%20s%.2f"%('Piwinski angle = ',sigSS/sigXS*np.tan(crossAS)))
    print("%20s%.2f, %0.2f"%('IP betas = ',betaIPx,betaIPy))
    print("%20s%.2e"%('ZNr/g= ',ZNR_G))
    print("%20s%.2e, %.2e"%('Weak sizes = ',sigXW,sigYW))
    print("%20s%.2e, %.2e"%('Strong sizes = ',sigXS,sigYS))
    print(indBatch)
    print(gamma)
    print('beam',lost_beam, 1/np.sqrt(lost_beamx),1/np.sqrt(lost_beamy))
    print('mach',lost_mach, 1/np.sqrt(lost_machx),1/np.sqrt(lost_machy))
    sys.stdout.flush()

    ##########################################################
    ################# Initialize particles ###################
    xAll, xpAll, yAll, ypAll,sAll,dAll,weight = mods.initial_distribution(Dist,nPart,maxA,intensity)
    Dist = int(Dist)
    
    inBeamAll = (xAll**2*lost_beamx + yAll**2*lost_beamy <= 1).astype(np.int32)
    inMachAll = (xAll**2*lost_machx + yAll**2*lost_machy <= 1).astype(np.int32)
    print(nPart,np.sum(inBeamAll), np.sum(inMachAll))
    jxAll = 0.5*(xAll**2+xpAll**2).astype(np.float64) 
    jyAll = 0.5*(yAll**2+ypAll**2).astype(np.float64) 
    jzAll = 0.5*(sAll**2+dAll**2).astype(np.float64) 
    smearxAll = np.zeros_like(xAll).astype(np.float64)
    smearyAll = np.zeros_like(xAll).astype(np.float64)
    
    nP = int(np.max(np.diff(indBatch)))
    xMat = np.zeros((nP,int(nSave))).astype(np.float64)  #: Matrices to save multiple turns of the first 4 dimensions
    yMat = np.copy(xMat)  
    if nQ == 1:
        xpMat= np.copy(xMat) ; ypMat = np.copy(xMat)

    QxAll = QyAll = 0   
    
    # Load arrays onto GPU
    xg = gp.to_gpu(xAll)   ;   xpg= gp.to_gpu(xpAll)
    yg = gp.to_gpu(yAll)   ;   ypg= gp.to_gpu(ypAll)
    sg = gp.to_gpu(sAll)   ;   dg = gp.to_gpu(dAll)
    
    inBeamg = gp.to_gpu(inBeamAll)
    inMachg = gp.to_gpu(inMachAll)

    jxg = gp.zeros(nPart,dtype = np.float64) 
    jyg = gp.to_gpu(jyAll)   ;   smearxg = gp.to_gpu(smearxAll)   ;   smearyg = gp.to_gpu(smearyAll)
    jzg = gp.to_gpu(jyAll) 
        
    xMatg = gp.zeros(xMat.shape,dtype = np.float64)  
    yMatg = xMatg.copy()     ;   xpMatg = xg.copy()     ;    ypMatg= xg.copy()
    if nQ == 1:
        xpMatg= xMatg.copy()   ;    ypMatg= xMatg.copy()

    
    ##########################################################
    ##################### TRACKING ###########################



    KERNEL1 = KERNEL.format(
        PI=PI,
        SQRPI2 = 2*np.sqrt(const.pi) ,
        # How to do it
        QX1 = Qx1, QX2 = Qx2, QP1=Qp1, 
        QY1 = Qy1, QY2 = Qy2, QP2=Qp2,
        COSX1 = np.cos(2*PI*Qx1), SINX1 = np.sin(2*PI*Qx1),
        COSX2 = np.cos(2*PI*Qx2), SINX2 = np.sin(2*PI*Qx2),
        COSY1 = np.cos(2*PI*Qy1), SINY1 = np.sin(2*PI*Qy1),
        COSY2 = np.cos(2*PI*Qy2), SINY2 = np.sin(2*PI*Qy2),
        COSS1 = np.cos(2*PI*Qs1) , SINS1 = np.sin(2*PI*Qs1) ,
        COSS2 = np.cos(2*PI*Qs2) , SINS2 = np.sin(2*PI*Qs2),
        SAW = np.sin(crossAW), CAW = np.cos(crossAW),TAW = np.tan(crossAW),
        SEPX = sepX , SEPY = sepY ,SEPN = sepIPn,
        HEADONC = np.float64(headonC), ZNR_G = ZNR_G,
        NOISEA = np.float64(noiseA), 
        LOST_BEAMX = lost_beamx, LOST_BEAMY = lost_beamy,
        LOST_MACHX = lost_machx, LOST_MACHY = lost_machy,
        # What to do         
        N = IPS_np[4],
        HO14D = IPS_np[0]*(nSlice==0), HO24D = IPS_np[2]*(nSlice==0),
        HO1SB = IPS_np[0]*(nSlice>0),  HO2SB = IPS_np[2]*(nSlice>0),
        CH   = 1*CH,
        LAT1 = LAT1,
        LAT2 = LAT2,
        CROSS2 = crossing2,
        # Beamsize
        NSLICE=nSlice,
        sliceX=mods.cstr(sliceX), sliceS =mods.cstr(sliceS),
        nPart = nPart,
        SIGXW=sigXW,      SIGPW=sigPW,      SIGYW=sigYW,      SIGQW=sigQW,     SIGSW=sigSW,SIGDW=sigDW,
        SIGXXS=sigXXS_HO, SIGYYS=sigYYS_HO, SIGPPS=sigPPS_HO, SIGQQS=sigQQS_HO,
        )
#    print(KERNEL1[KERNEL1.find('TRACKING')-12:KERNEL1.find('PROCESSSING')])
    
    
    modGPU = comp.SourceModule(KERNEL1 , no_extern_c = True)
    trackGPU = modGPU.get_function("trackGPU")
    mtpb = mods.meminfo(trackGPU) 
    if mtpb< threads:
        threads = mtpb
        print('OBSOBS: MAX THREADS PER BLOCK IS %d'%threads)
    
    argsInt = gp.zeros((5,),dtype=np.int32) #: argsInt = [turnA, turnZ, nSave,nMean,nP]
    
    streamTrack= cuda.Stream()              #: stream to track - asynchronous behaviour
    streamCopy = cuda.Stream()              #: stream to copy  - asynchronous behaviour
    
    iSnap = 0                               #: to be updated: which snapshot is this
    iTurn = 0                               #: to be updated: which turn is this snapshot after
    iQ  =  0                                #: to be updated: which tune is this
    indGroup=np.zeros(1).astype(np.int32)   #: to be updated: indices of groups (smaller than batch, to get tune)
    nGroup = 1                              #: to be updated: total # of groups
    start2 = time.time()    
    
    

    # Start tracking
    for i in range(sizeTurnArr):            #: Take turnArr[i] turns at a time
        iTurn = np.sum(turnArr[0:i+1])
        iQ += 1*(bool_tuneArr[i]) 
        
        if turnArr[i] >0 :
            group = 0
            print("Turn %7d of %7d"%(iTurn,np.sum(turnArr)),end="")
            sys.stdout.flush()
            for batch in range(nBatch):
                i1 = indBatch[batch]    ;    i2 = indBatch[batch+1]
                nP = int(i2-i1)                                                 #: # particles in this batch
                gridSz = np.ceil(nP*1./threads).astype(np.int32)                #: Size of grid 
                nT = max(nSave,nMean) if bool_tuneArr[i] else turnArr[i]        #: # turns to be tracked initially
                argsInt[:] = np.int32([0 , nT, nSave*bool_tuneArr[i], min(nMean,turnArr[i]),nP ]) 

                # Initial tracking 
                trackGPU(xg[i1:i2],xpg[i1:i2],yg[i1:i2],ypg[i1:i2],sg[i1:i2],dg[i1:i2],
                            inBeamg[i1:i2],inMachg[i1:i2],
                            jxg[i1:i2],smearxg[i1:i2],jyg[i1:i2],smearyg[i1:i2],
                            jzg[i1:i2],
                            xMatg,yMatg,xpMatg,ypMatg,argsInt,
                            grid=(int(gridSz),1,1),block=(threads,1,1))#,stream = streamTrack)

                # Additional tracking for simultaneous tracking and copying
                if bool_tuneArr[i] :
                    
                    cpy_xMatg = xMatg.copy() 
                    cpy_yMatg = yMatg.copy() 
                    if nQ == 1:
                        cpy_xpMatg = xpMatg.copy() 
                        cpy_ypMatg = ypMatg.copy() 

                    # Additional tracking asynchronoysly ↓ 
                    if turnArr[i]>nSave:
                        argsInt[:] = np.int32([nT, turnArr[i], nSave, min(nMean,turnArr[i]),nP ]) 
                        trackGPU(xg[i1:i2],xpg[i1:i2],yg[i1:i2],ypg[i1:i2],sg[i1:i2],dg[i1:i2],
                            inBeamg[i1:i2],inMachg[i1:i2],
                            jxg[i1:i2],smearxg[i1:i2],jyg[i1:i2],smearyg[i1:i2],
                            jzg[i1:i2],
                            xMatg,yMatg,xpMatg,ypMatg,argsInt,
                            grid=(int(gridSz),1,1),block=(threads,1,1),stream = streamTrack)
                    
                    #Copy big matrixes asynchronously ↑
                    cuda.memcpy_dtoh_async(xMat ,cpy_xMatg.ptr ,stream = streamCopy)
                    cuda.memcpy_dtoh_async(yMat ,cpy_yMatg.ptr ,stream = streamCopy)
                    if nQ == 1:
                        cuda.memcpy_dtoh_async(xpMat ,cpy_xpMatg.ptr ,stream = streamCopy)
                        cuda.memcpy_dtoh_async(ypMat ,cpy_ypMatg.ptr ,stream = streamCopy)
                    
                    # Synchronize streams again - Makes sure every operation is completed
                    context.synchronize()
                    
                    #######################################################
                    ########### Save tunemat or calculate tunes ###########
                    if bool_savePlotPkl and bool_tuneArr[i] and bool_tune and True:
                        # Save matrices to calculate tunes later
                        if True or nQ == 1:
                            indLoc = np.append(np.arange(0,i2-i1,int((i2-i1)/min(i2-i1,10))),i2-i1).astype(np.int32)
                            for ind in range(indLoc.size-1):
                                j1 = indLoc[ind]  ; j2 = indLoc[ind+1]
                                with open(savedir+'/multsnap_iQ%d_iG%d.p'%(iQ,group),'wb') as pklfile:
                                    pkl.dump([xMat[j1:j2,:],yMat[j1:j2,:]],pklfile) 
                                if nQ == 1:
                                    with open(savedir+'/multsnapP_iQ%d_iG%d.p'%(iQ,group),'wb') as pklfile:
                                        pkl.dump([xpMat[j1:j2,:],ypMat[j1:j2,:]],pklfile)
                                group+=1
                            
                        #Calculate tunes right away 
                        else:
                            if batch ==0:
                                QxAll = np.zeros_like(xAll)
                                QyAll = np.zeros_like(xAll)
                                
                            # Calculate tunes in tuneDistbeam.py
                            indLoc  = modTune.calculateTunesDirectly(i1,i2,nSave,XI,Qx0,Qy0,Qp1+Qp2,Qs,
                                                                    xMat,yMat,QxAll,QyAll)
                            if batch == nBatch-1:
                                print(": Tunes Calculated", end = "")
                        if iQ ==1:
                            indGroup = np.append(indGroup,i1 + indLoc[1:])
                            nGroup = group


            # Transfer x,xp, y,yp, jx,smearx,jy,smeary, inbeam, after all batches have been tracked 

            cuda.memcpy_dtoh(xAll ,xg.ptr)          ;    cuda.memcpy_dtoh(xpAll,xpg.ptr)
            cuda.memcpy_dtoh(yAll ,yg.ptr)          ;    cuda.memcpy_dtoh(ypAll,ypg.ptr)
            cuda.memcpy_dtoh(sAll ,sg.ptr)          ;    cuda.memcpy_dtoh(dAll, dg.ptr)
            cuda.memcpy_dtoh(jxAll    ,jxg.ptr)     ;    cuda.memcpy_dtoh(smearxAll,smearxg.ptr)
            cuda.memcpy_dtoh(jyAll    ,jyg.ptr)     ;    cuda.memcpy_dtoh(smearyAll,smearyg.ptr)
            cuda.memcpy_dtoh(jzAll    ,jzg.ptr)     ;
            cuda.memcpy_dtoh(inBeamAll,inBeamg.ptr) ;    cuda.memcpy_dtoh(inMachAll,inMachg.ptr)            
            print(": Done - %d min, %.3f sek"%(np.floor((time.time()-start2)/60),np.mod((time.time()-start2),60)))
        
        sys.stdout.flush()
        
        #################################
        ########### Save snap ###########
        if bool_savePlotPkl:
            if i==0: 
                snapPrev = [iTurn,xAll.copy(),xpAll.copy(),yAll.copy(),ypAll.copy(),sAll.copy(),dAll.copy(),
                            weight.copy(),inBeamAll>0.5, inMachAll>0.5 ,0, QxAll+0,QyAll+0,
                            jxAll.copy(),jyAll.copy(),smearxAll.copy(),smearyAll.copy(),jzAll.copy()]
            if i>0:
                snapPrev[10:] = [iQ*bool_tuneArr[i],QxAll+0,QyAll+0,
                                jxAll.copy(),jyAll.copy(),smearxAll.copy(),smearyAll.copy(),jzAll.copy()]
                iSnap += 1
                with  open(savedir+'/snap_iS%d.p'%(iSnap),'wb') as pklfile: 
                    pkl.dump(snapPrev, pklfile)
                snapPrev[:10] = [iTurn,xAll.copy(),xpAll.copy(),yAll.copy(),ypAll.copy(),sAll.copy(),dAll.copy(),
                            weight.copy(),inBeamAll>0.5, inMachAll>0.5 ]
    
        
    #######################################################
    ################## Save parameters ####################
    if bool_savePlotPkl:
        with open(savedir+'/parameters.p','wb') as pklfile:
            parameters = [IPS,Qx0,Qy0,Qx1,Qy1,XI,intensity,gamma,crossAS,betaIPx, #betaIPy, 
            emitNXW,emitNYW,emitNXS,emitNYS,Qs,Qs1, ZNR_G, lost_beam,lost_mach,
            headonC, nLR, longrangeC, normsepLR, noiseA,sigSW,sigDW,Qp,Qp1, maxA,nPart,inTurn,midTurn,ln2turn,
            nSave,nMean,nQ,Dist,nGroup,indGroup,turnArr,bool_savePlotPkl,bool_tuneArr,bool_tune]
            pkl.dump(parameters,pklfile)
    



################################################
################# KERNEL #######################
if int(sys.argv[10]) ==1 :
    _saveTurns = """ 
            if(turn<nSave){{xMat[ i*nSave + turn] = x[i]  ;
                            yMat[ i*nSave + turn] = y[i]  ;
                            xpMat[ i*nSave + turn] = xp[i]  ;
                            ypMat[ i*nSave + turn] = yp[i]  ;     }} """

else:
    _saveTurns = """ 
            if(turn<nSave){{xMat[ i*nSave + turn] = x[i]  ;
                            yMat[ i*nSave + turn] = y[i]  ;     }} """
_avgAction = """
        ////////////////////////////////////////
        /// Make average of first nMean turns ///

        
        if ( turn < nMean){{
            temp      = (x[i]*x[i] + xp[i]*xp[i])/2.0 ;
            jx[i]    += temp                 ;
            smearx[i]+= temp*temp           ; 

            
            temp      = (y[i]*y[i] + yp[i]*yp[i])/2.0 ;
            jy[i]    += temp                ;
            smeary[i]+= temp*temp           ;
            
            temp      = (s[i]*s[i] + d[i]*d[i])/2.0 ;
            jz[i]    += temp                        ;
            
            if (turn == (nMean-1)){{
                jx[i] = jx[i]/nMean         ;
                jy[i] = jy[i]/nMean         ;    
                jz[i] = jz[i]/nMean         ;
                smearx[i] = sqrt(smearx[i]/nMean-jx[i]*jx[i]) ; 
                smeary[i] = sqrt(smeary[i]/nMean-jy[i]*jy[i]) ;
            }}
        }} """

_werf = """
/////////////////////////////////////////////////////////////////////////////
//
// DATE
//   06/22/2015
//
// AUTHORS
//   Hannes Bartosik, Adrian Oeftiger
//
// DESCRIPTION
//   FADDEEVA error function for GPU in CUDA.
//   This file is intended to be used as a
//   preamble to depending kernels, e.g. in PyCUDA
//   via ElementwiseKernel(..., preamble=open( <this_file> ).read()).
//
/////////////////////////////////////////////////////////////////////////////

#define errf_const 1.12837916709551
#define xLim 5.33
#define yLim 4.29

__device__ void inline wofz(double in_real, double in_imag,
                     double* out_real, double* out_imag)
{{
    /**
    this function calculates the double precision complex error function
    based on the algorithm of the FORTRAN function written at CERN by
    K. Koelbig, Program C335, 1970.
    See also M. Bassetti and G.A. Erskine, "Closed expression for the
    electric field of a two-dimensional Gaussian charge density",
    CERN-ISR-TH/80-06.
    */

    int n, nc, nu;
    double h, q, Saux, Sx, Sy, Tn, Tx, Ty, Wx, Wy, xh, xl, x, yh, y;
    double Rx [33];
    double Ry [33];

    x = fabs(in_real);
    y = fabs(in_imag);

    if (y < yLim && x < xLim) {{
        q = (1.0 - y / yLim) * sqrt(1.0 - (x / xLim) * (x / xLim));
        h  = 1.0 / (3.2 * q);
        nc = 7 + int(23.0 * q);
        xl = pow(h, double(1 - nc));
        xh = y + 0.5 / h;
        yh = x;
        nu = 10 + int(21.0 * q);
        Rx[nu] = 0.;
        Ry[nu] = 0.;
        for (n = nu; n > 0; n--){{
            Tx = xh + n * Rx[n];
            Ty = yh - n * Ry[n];
            Tn = Tx*Tx + Ty*Ty;
            Rx[n-1] = 0.5 * Tx / Tn;
            Ry[n-1] = 0.5 * Ty / Tn;
            }}
        Sx = 0.;
        Sy = 0.;
        for (n = nc; n>0; n--){{
            Saux = Sx + xl;
            Sx = Rx[n-1] * Saux - Ry[n-1] * Sy;
            Sy = Rx[n-1] * Sy + Ry[n-1] * Saux;
            xl = h * xl;
        }};
        Wx = errf_const * Sx;
        Wy = errf_const * Sy;
    }}
    else {{
        xh = y;
        yh = x;
        Rx[0] = 0.;
        Ry[0] = 0.;
        for (n = 9; n>0; n--){{
            Tx = xh + n * Rx[0];
            Ty = yh - n * Ry[0];
            Tn = Tx * Tx + Ty * Ty;
            Rx[0] = 0.5 * Tx / Tn;
            Ry[0] = 0.5 * Ty / Tn;
        }};
        Wx = errf_const * Rx[0];
        Wy = errf_const * Ry[0];
    }}

    if (y == 0.) {{
        Wx = exp(-x * x);
    }}
    if (in_imag < 0.) {{
        Wx =   2.0 * exp(y * y - x * x) * cos(2.0 * x * y) - Wx;
        Wy = - 2.0 * exp(y * y - x * x) * sin(2.0 * x * y) - Wy;
        if (in_real > 0.) {{
            Wy = -Wy;
        }}
    }}
    else if (in_real < 0.) {{
        Wy = -Wy;
    }}

    *out_real = Wx;
    *out_imag = Wy;
}}
"""

_symBeamMap = """
//////////////////////////////////////////////
///////////////// SymBeamMap /////////////////
//////////////////////////////////////////////

// **************************************************
__device__ void inline NORM2ACC(double* x, double* y, double* s,
                                double* xp,double* yp,double* d, int i){{ 
    x[i] = x[i]*{SIGXW}      ;
    xp[i]= xp[i]*{SIGPW}     ;
    y[i] = y[i]*{SIGYW}      ;
    yp[i]= yp[i]*{SIGQW}     ;
    s[i] = s[i]*{SIGSW}      ;
    d[i] = d[i]*{SIGDW}      ;
}} // end NORM2ACC

__device__ void inline ACC2NORM(double* x, double* y, double* s,
                                double* xp,double* yp,double* d, int i){{ 
    x[i] = x[i]/{SIGXW}      ;
    xp[i]= xp[i]/{SIGPW}     ;
    y[i] = y[i]/{SIGYW}      ;
    yp[i]= yp[i]/{SIGQW}     ;
    s[i] = s[i]/{SIGSW}      ;
    d[i] = d[i]/{SIGDW}      ;
}} // end ACC2NORM


__device__ void inline BOOST(double* x,double* y,double* s,
                      double* xp,double* yp,double* d,
                      const double SAW,const double CAW,const double TAW, int i){{
    double A,SQR1A,H,HSQR,H1DER, xl     ;
    
    A = (xp[i]*xp[i]+yp[i]*yp[i])/pow(1.0 + d[i],2.0) ;
    SQR1A = sqrt(1-A)                   ;
    H = (d[i]+1)*A/(1+SQR1A)            ;
    
    xp[i] = (xp[i]-TAW*H)/CAW           ;
    yp[i] = yp[i]/CAW                   ;
    d[i]  = d[i] - SAW*xp[i]            ;
    
    A = (xp[i]*xp[i]+yp[i]*yp[i])/pow(1.0 + d[i],2.0) ;
    SQR1A = sqrt(1-A)                   ;
    HSQR = (1+d[i])*SQR1A               ;
    
    H1DER = xp[i]/HSQR                  ;  // dh*/dx
    xl = TAW*s[i] + (1+SAW*H1DER)*x[i]  ;
    H1DER = yp[i]/HSQR                  ;  // dh*/dy
    y[i] = y[i] + SAW*H1DER*x[i]        ;
    H1DER = A/(1+SQR1A)/SQR1A           ;  // -dh*/ds
    s[i] = s[i]/CAW - SAW*H1DER*x[i]    ;
    x[i] = xl                           ;
}} //end BOOST

__device__ void inline BOOST_INVERSE(double* x,double* y,double* s,
                              double* xp,double* yp,double* d,
                              const double SAW,const double CAW, int i){{
    double A,SQR1A,H,HSQR,H1X,H1Y,H1Z,DET     ;
    
    A = (xp[i]*xp[i]+yp[i]*yp[i])/pow(1.0 + d[i],2.0) ;
    SQR1A = sqrt(1-A)                   ;
    HSQR = (1+d[i])*SQR1A               ;
    H = (d[i]+1)*A/(1+SQR1A)            ;
    
    H1X = xp[i]/HSQR                    ;  // dh*/dx
    H1Y = yp[i]/HSQR                    ;  // dh*/dy
    H1Z = A/(1+SQR1A)/SQR1A             ;  // -dh*/ds
    DET = 1+SAW*(H1X - SAW*H1Z)         ;

    x[i] = (x[i]-SAW*s[i])/DET          ;
    y[i] = y[i] - SAW*H1Y*x[i]          ;  // Must come after x* -> x
    s[i] = CAW*(s[i] + H1Z*SAW*x[i])    ;

    d[i]  = d[i] + SAW*xp[i]            ;  // Must come before xp* -> xp
    xp[i] = (xp[i]+SAW*H)*CAW           ;
    yp[i] = yp[i]*CAW                   ;
}} //end BOOST_INVERSE


__device__ void inline NOTROUNDBBF(double sepX,double sepY,double sigXXS_S,double sigYYS_S,
                                    double sigPPS,double sigQQS,double S,
                                    double* BBFX,double* BBFY,double* G){{
    double R2 , FAC2 , FAC , CONST , SIGXY, ARG1X,ARG1Y , EXPFAC        ;
    double WX1 , WY1 , WX2, WY2            ;
        
    R2 = sepX*sepX/sigXXS_S + sepY*sepY/sigYYS_S   ;
    FAC2 = 2*fabs(sigXXS_S-sigYYS_S)         ;
    FAC = sqrt(FAC2)                         ;
    CONST = SQRPI2/FAC                       ;
    SIGXY = sqrt(sigXXS_S/sigYYS_S)          ;
    ARG1X = fabs(sepX/FAC)                   ;
    ARG1Y = fabs(sepY/FAC)                   ;
    wofz(ARG1X,ARG1Y , &WY1,&WX1 )          ; // (inreal, inimag,outreal,outimag)
    if (R2 < 100.0) {{
        EXPFAC = exp(-R2*0.5)               ;
        wofz(ARG1X/SIGXY , ARG1Y*SIGXY, &WY2,&WX2 ) ;
        *BBFX = CONST*(WX1-EXPFAC*WX2)       ;
        *BBFY = CONST*(WY1-EXPFAC*WY2)       ;
        if (sepX<0.0){{ *BBFX = -(*BBFX) ; }}
        if (sepY<0.0){{ *BBFY = -(*BBFY) ; }}
        
        CONST = *BBFX*sepX + *BBFY*sepY   ;
        *G = S*(-sigPPS*((CONST+2*(EXPFAC/SIGXY-1))/FAC2) +
                 sigQQS*((CONST+2*(EXPFAC*SIGXY-1))/FAC2))    ;
    }} else {{
        *BBFX = CONST*WX1                ;
        *BBFY = CONST*WY1                ;
        if (sepX<0.0){{ *BBFX = -(*BBFX) ; }}
        if (sepY<0.0){{ *BBFY = -(*BBFY) ; }}
        
        CONST = *BBFX*sepX + *BBFY*sepY   ;
        *G = S*(-sigPPS+sigQQS)*(CONST-2)/FAC2 ;
    }}
    *BBFX *= (-{ZNR_G})   ;
    *BBFY *= (-{ZNR_G})   ;
    *G    *= (-{ZNR_G})   ;
}} // end NOTROUNDBBF


__device__ void inline SBC(double* x,double* y,double* s,
                              double* xp,double* yp,double* d, int HC,
                              int i){{
    double S, sepX, sepY, sigXXS_S, sigYYS_S,R2  ;
    double BBFX, BBFY, G                         ;
    
    for(int iSlice = 0; iSlice<{NSLICE} ; iSlice++){{
        S = (s[i] - SLICES[iSlice])*0.5         ;
        sepX = x[i] + xp[i]*S -    HC *SLICET[iSlice] + (1-HC)*{SEPX} ;
        sepY = y[i] + yp[i]*S - (1-HC)*SLICET[iSlice] +    HC *{SEPY} ; 
        R2 = sepX*sepX + sepY*sepY             ;
        sigXXS_S = {SIGXXS} + {SIGPPS} *S*S    ;
        sigYYS_S = {SIGYYS} + {SIGQQS} *S*S    ;
        
        
        if (fabs(sigXXS_S-sigYYS_S)<1e-9*sigXXS_S) {{ // 
        // if (sigXXS_S == sigYYS_S) {{
        // if round beams : 
            if (R2 != 0 ){{
                G = -1*({ZNR_G})/R2 *2.0 *(1-exp(-0.5*R2/sigXXS_S))  ;
                BBFX = sepX*G  ;
                BBFY = sepY*G  ;
            }} else {{ 
                BBFX = 0 ;
                BBFY = 0 ;
            }}
            G = -S*({ZNR_G})*({SIGPPS}+{SIGQQS})*0.5/sigXXS_S * exp(-0.5*R2/sigXXS_S) ;
            
        }} else {{
        // If not round beams
            if (sigXXS_S > sigYYS_S){{
                NOTROUNDBBF(sepX,sepY,sigXXS_S,sigYYS_S,{SIGPPS},{SIGQQS},S, &BBFX, &BBFY, &G) ;
            }} else {{ 
                NOTROUNDBBF(sepY,sepX,sigYYS_S,sigXXS_S,{SIGQQS},{SIGPPS},S, &BBFY, &BBFX, &G) ;
            }}
        }}
        
        // Apply the kicks
        d[i] += - 0.5*BBFX*(xp[i]-0.5*BBFX) - 0.5*BBFY*(yp[i]-0.5*BBFY) - G  ;
        x[i] += S*BBFX                       ;
        xp[i]-= BBFX                         ;
        y[i] += S*BBFY                       ;
        yp[i]-= BBFY                         ;
    }} //end for    
}} // end SBC


__device__ void B_SBC_BI(double* x, double* y, double* s,
                         double* xp,double* yp,double* d, int HC, int i){{
    if(HC>0.5) {{
        BOOST(x,y,s,xp,yp,d,{SAW},{CAW},{TAW},i)       ;
        SBC(x,y,s,xp,yp,d,HC,i)                           ;
        BOOST_INVERSE(x,y,s,xp,yp,d,{SAW},{CAW},i)     ;
    }} else {{
        BOOST(y,x,s,yp,xp,d,{SAW},{CAW},{TAW},i)       ;
        SBC(x,y,s,xp,yp,d,HC,i)                         ;
        BOOST_INVERSE(y,x,s,yp,xp,d,{SAW},{CAW},i)     ;
    }}
    
}} //end B_SBC_BI
"""


KERNEL = """
#include <curand.h>
#include <curand_kernel.h>
#include <stdio.h>
#include <math.h>

extern "C" {{

__constant__ double PI = 3.1415926535897932     ;
__constant__ double SQRPI2 = {SQRPI2}           ;
__constant__ double SLICET[] = {sliceX}            ;
__constant__ double SLICES[] = {sliceS}            ;
__constant__ int NP = {nPart}                      ;

""" +_werf + _symBeamMap + """

///////////////////////////////////////////////
///////////////// LATTICE+4D↓ /////////////////
///////////////////////////////////////////////

__device__ inline void _headon(double* x, double* xp, double* y, double* yp,int hC, int i){{
    double KX=0.0 , KY=0.0       ;
    
    // Apply separation
    y[i] +=    hC *{SEPN}   ;
    x[i] += (1-hC)*{SEPN}   ;
    
    double temp = x[i]*x[i]*{SIGXW}*{SIGXW}/{SIGXXS}   +    y[i]*y[i]*{SIGYW}*{SIGYW}/{SIGYYS}        ;
    if (temp != 0){{
        if (fabs({SIGXXS}-{SIGYYS})<1e-9*{SIGXXS}) {{
        // if ({SIGXXS} == {SIGYYS}) {{  
            // Round beams
            KX = -{HEADONC}*(1-exp(-temp*0.5))*x[i]/temp   ;
            KY = -{HEADONC}*(1-exp(-temp*0.5))*y[i]/temp   ;
        }} else {{ 
            // Flat beams
            if ({SIGXXS} > {SIGYYS} ){{
                NOTROUNDBBF(x[i]*{SIGXW},y[i]*{SIGYW},{SIGXXS},{SIGYYS},{SIGPPS},{SIGQQS},0, &KX, &KY, &temp) ;
            }} else {{ 
                NOTROUNDBBF(y[i]*{SIGYW},x[i]*{SIGXW},{SIGYYS},{SIGXXS},{SIGQQS},{SIGPPS},0, &KY, &KX, &temp) ;
            }}
            KX  = KX/{SIGPW} ;
            KY  = KY/{SIGQW} ;
        }}
    }} else {{ 
        // dead center
        KX=0 ;
        KY=0 ;
    }}
     
    
    // Apply kicks (note sign)
    xp[i] -=KX              ;
    yp[i] -=KY              ; 
    // Remove separation again
    y[i] -=    hC *{SEPN}   ;
    x[i] -= (1-hC)*{SEPN}   ;
}} // end _headon

__device__ inline void _rotate(double* q, double* qp, double C, double S, int i){{  
    double temp = q[i]            ;
    q[i]   = C*temp  + S*qp[i]    ;
    qp[i]  =-S*temp  + C*qp[i]    ; 
}} // end _rotate

__device__ inline void _setCS(double* C, double* S, double* d, double Q, double Qp, double C0, double S0, int i){{
    if({CH}==0){{
        *C = C0 ;
        *S = S0 ;        
    }}else{{
        double temp = Q+d[i]*{SIGDW}*Qp   ;
        *C = cos(2*PI*temp)  ;
        *S = sin(2*PI*temp)  ;
    }}
}} // end _setCS

__device__ inline void _noise(double* xp, double* yp,curandState* curs, int i ){{
    xp[i] += {NOISEA}*curand_normal(curs);
    yp[i] += {NOISEA}*curand_normal(curs);
}} // end noise

__device__ inline void _lost(double* x, double* y, int* inbeam, int* inmach, int i){{
    double x2 = x[i]*x[i]   ;
    double y2 = y[i]*y[i]   ;
    if( x2*{LOST_BEAMX} + y2*{LOST_BEAMY} > 1){{
        inbeam[i] = 0 ;
    }}
    if( x2*{LOST_MACHX} + y2*{LOST_MACHY} > 1){{
        inmach[i] = 0 ;
    }}
}} // end lost





//////////////////////////////////////////
///////////////// MAIN ↓ /////////////////
//////////////////////////////////////////
__global__ void trackGPU(double* x, double* xp, 
                        double* y, double* yp,
                        double* s, double* d, 
                        int* inbeam, int* inmach,
                        double* jx, double* smearx,
                        double* jy, double* smeary,
                        double* jz,
                        double* xMat, double* yMat,
                        double* xpMat, double* ypMat,
                        int*  argsInt){{
for (int i = blockIdx.x * blockDim.x + threadIdx.x ; 
         i < argsInt[4] ; 
         i += blockDim.x * gridDim.x){{


    const int turnA  = argsInt[0]           ;
    const int turnZ  = argsInt[1]           ; //[turnA, turnZ, nSave, nMean, nP]
    const int nSave  = argsInt[2]           ;
    const int nMean  = argsInt[3]           ;
    
    double C,S,temp ;

    /////////////////////////////////////
    // Initialize random number generator
    curandState curs                    ;
    if({N}>0){{         curand_init (clock64() , i, 0, &curs)  ;    }}
    
    if( turnA == 0 ){{
        jx[i]=0.0 ;
        jy[i]=0.0 ;
        jz[i]=0.0 ;
        smearx[i]=0.0 ;
        smeary[i]=0.0 ;
    }}
    
    ////////////////
    /// Tracking ///
    for(int turn = turnA ; turn< turnZ ; turn++){{
    
        ////////TRACKING ////////////
        if({HO1SB} ){{     NORM2ACC(x,y,s,xp,yp,d,i)     ;
                 //          BOOST(x,y,s,xp,yp,d,{SAW},{CAW},{TAW},i)       ;
                 //          SBC(x,y,s,xp,yp,d,1,i)                         ;
                 //          BOOST_INVERSE(x,y,s,xp,yp,d,{SAW},{CAW},i)     ;
                           B_SBC_BI(x,y,s,xp,yp,d,1,i)   ;   // Horizontal crossing  [-2] = 1
                           ACC2NORM(x,y,s,xp,yp,d,i)     ;  }} 
        else if({HO14D}){{ _headon(x,xp,y,yp,1,i)        ; }}
        if({LAT1}){{       _rotate(s,d,{COSS1},-{SINS1}, i)                    ;  
                           _setCS(&C, &S, d,{QX1},{QP1},{COSX1},{SINX1},i)   ;
                           _rotate(x,xp,C,S, i)                               ;
                           _setCS(&C, &S, d,{QY1},{QP1},{COSY1},{SINY1},i)   ;
                           _rotate(y,yp,C,S, i )                              ;  }}
        if({HO2SB} ){{     NORM2ACC(x,y,s,xp,yp,d,i)     ;
            //               BOOST(y,x,s,yp,xp,d,{SAW},{CAW},{TAW},i)       ;
            //               SBC(x,y,s,xp,yp,d,0,i)                         ;
            //               BOOST_INVERSE(y,x,s,yp,xp,d,{SAW},{CAW},i)     ;
                           B_SBC_BI(x,y,s,xp,yp,d,{CROSS2},i)   ;  // Vertical crossing [-2] = 0
                           ACC2NORM(x,y,s,xp,yp,d,i)     ;  }}    
        else if({HO24D}){{ _headon(x,xp,y,yp,{CROSS2},i)        ; }}        
        if({LAT2}){{       _rotate(s,d,{COSS2},-{SINS2}, i)                    ;  
                           _setCS(&C, &S, d,{QX2},{QP2},{COSX2},{SINX2},i)   ;
                           _rotate(x,xp,C,S, i)                               ;
                           _setCS(&C, &S, d,{QY2},{QP2},{COSY2},{SINY2},i)   ;
                           _rotate(y,yp,C,S, i )                              ;  }}

        if({N}   ){{       _noise(xp,yp,&curs,i)                               ;  }}
                       
        //////// PROCESSSING ////////////
        _lost(x,y,inbeam,inmach,i)   ;        
        """+ _saveTurns +"""
        """+ _avgAction +"""

    }} // end tracking for loop
}} // end i for loop
}} // end trackGPU

}} // end external C

"""
#if(i ==1 && turn ==1 ){{printf("CH ") ;}}



KERNEL_TEST = """
#include <stdio.h>
#include <math.h>

extern "C" {{

__constant__ double PI = 3.1415926535897932     ;
__constant__ double SQRPI2 = {SQRPI2}           ;
__constant__ double SLICET[] = {sliceX}            ;
__constant__ double SLICES[] = {sliceS}            ;
__constant__ int NP = {nPart}                      ;

""" +_werf + _symBeamMap + """
//////////////////////////////////////////
///////////////// MAIN ↓ /////////////////
//////////////////////////////////////////
__global__ void trackGPUtestNSLICE(double* x, double* xp, 
                        double* y, double* yp,
                        double* s, double* d){{
for (int i = blockIdx.x * blockDim.x + threadIdx.x ; 
         i < NP ; 
         i += blockDim.x * gridDim.x){{


    NORM2ACC(x,y,s,xp,yp,d,i)                      ;
    BOOST(x,y,s,xp,yp,d,{SAW},{CAW},{TAW},i)       ;
    SBC(x,y,s,xp,yp,d,1,i)                         ;
    BOOST_INVERSE(x,y,s,xp,yp,d,{SAW},{CAW},i)     ;
    ACC2NORM(x,y,s,xp,yp,d,i)                      ;
}} // end i for loop
}} // end trackGPU

}} // end external C

"""



#############################################################
#############################################################

#sys.argv = [sys.argv[0],"101010",".310",".320","0.01","0","100","0","10","1","2","311","0","2"]
def main():
    mods.initialize()
    
    weakstrong()
    
    
    
if __name__ == '__main__':
    start1 = time.time()
    main()
    print(r'%d min, %.3f sek'%(np.floor((time.time()-start1)/60),np.mod((time.time()-start1),60)))
    
    
    
    
    
    
    

#                            if batch ==0:
#                                QxAll = np.zeros_like(xAll)
#                                QyAll = np.zeros_like(xAll)
#                                          
#                            indLoc = np.linspace(0,i2-i1,nProcs+1).astype(np.int32)

#                            def collect_result(res):
#                                results.append(res) 
#                                
#                            #Paralellisation
#                            tune_method = 2
#                            if tune_method ==1 :
#                                Sussix().sussix_inp(nt1 = 1, nt2 =nSave,idam=2,ir=1,tunex=Qx0,tuney=Qy0)  #Write sussix input file

#                            results = []
#                            pool = mp.Pool(processes = nProcs)
#                            start3 = time.time()
#                            for ind in range(indLoc.size-1):  
#                                j1 = indLoc[ind]  ; j2 = indLoc[ind+1]            
#                                argsTune = [iQ,ind,start3,bool_stopstdout,tune_method,savedir,indLoc,nSave,nQ,indLoc.size-1,Qx0,Qy0,
#                                xMat[j1:j2,:],xpMat[j1:j2,:],yMat[j1:j2,:],ypMat[j1:j2,:]]
#                                p = pool.apply_async(modTune.groupSuss,args = (argsTune,),callback = collect_result)
#                                group+=1
#                            pool.close()
#                            pool.join()
#                            # Collect results
#                            for ind in range(indLoc.size-1):
#                                QxSome,QySome,j1,j2 = results[ind]
#                                j1 = j1+i1 ; j2 = j2+i1
#                                QxAll[j1:j2] = QxSome
#                                QyAll[j1:j2] = QySome
#                                
#                            if iQ ==1:
#                                indGroup = np.append(indGroup,i1 + indLoc[1:])
#                                nGroup = group
#                            if batch == nBatch-1:
#                                print(": Tunes Calculated", end = "")

