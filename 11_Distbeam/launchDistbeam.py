import os,sys
import moduleSondre as mods
import numpy as np
import datetime as dt
import pickle as pkl
import time
global sys1, runNow,filename1,boolplot1 , run,runs ,maxjobs,jobnr,scanType
runNow = False
sys1=[''] ; boolplot1=False ; filename1='abc'
scanType =4 # tunescan , parameterscan , error in postresults
job = 3
run = 1  # Run simulations
watch=0 # If want to watch the job
runs =2  # Run 1 or 2 jobs at same node simultaneous
maxjobs = 1
jobnr = 0
def run_program(sysargv,filename, bool_showprog, bool_plot):
#    return 0
    global sys1,runNow,filename1,boolplot1,runs,maxjobs,jobnr
    a,b = mods.noOutput(True)                    
    gpu_bool = os.system("nvidia-smi")
    if bool_showprog : 
        mods.noOutput(False,a,b)
        bool_showprog = True
    if gpu_bool==0:
        pythonvs = "python3 11_Distbeam/GPU"
    else:
        pythonvs = "python4 11_Distbeam/CPU"
#        print('NO GPU or NTNU')


    # NTNU or CERN
    pypath = os.environ['PYTHONPATH']
    if pypath.find('sondrevf')>=0 and True and not watch:
        #NTNU
        
#        NTNUQ = ['titanx','epic'][0] # Controlled in "subjob"
        if not runNow and runs==2:
            filename1 = filename
            boolplot1 = bool_plot
            sys1 = sysargv[:]
            sys1[1]='4'+sys1[1][1:]

            runNow = True
            c = 0
        else:
            sys.stdout.flush()
            command = ('qsub  -v "par1=\' '+' '.join([x for x in sysargv[1:]])+'\' ' +
                               ', par2=\' '+' '.join([x for x in sys1[1:]])+'\' ' +
                               ', bool_plot1='+str(bool_plot*1)  +
                               ', bool_plot2='+str(boolplot1*1)  +
                               ', filename1='+filename +
                               ', filename2='+filename1 +
                               ', runs='+str(runs)+
                               '" 11_Distbeam/subjob%s.sh'%['','','','','Titan'][scanType-1])
            if bool_showprog: print(command)
            c = os.system(command)
            jobnr +=1
            while not os.path.isfile('02_pickle_save/'+filename+'/post_done.txt') and jobnr >= maxjobs:
                time.sleep(30)
            if jobnr>= maxjobs: jobnr =0
            runNow = False
    else:
        #CERN
        c = os.system(pythonvs+'trackDistbeam.py '+' '.join([x for x in sysargv[1:]]))
        nQ = int(sysargv[10])
        NPart = int(eval(sysargv[6]))
        if nQ>1.5 or NPart>=1000:
            if (c != 1024) :
                os.system( 'python3 11_Distbeam/tuneDistbeam.py ' + filename \
                + ' && python3 11_Distbeam/postDistbeam.py ' + filename )
            
            if bool_plot and c != 1024 :
                os.system('python3 11_Distbeam/plotDistbeam.py ' + filename + ' 0011' ) 
        else:
            os.system( 'python3 11_Distbeam/specialTuneDistbeam.py ' + filename)
    if not bool_showprog: #np.size(vals1)*np.size(vals2)>4.5:
        mods.noOutput(False,a,b)    
    
    return c


############################################################################################
############################################################################################
############################################################################################
def main():
    global runNow , run,scanType
    NPAR = np.size(sys.argv)
    if NPAR<15:
        print("Not enough arguments given \nIPS Qx Qy XI maxA N iT mT p nQ Dist_Y_P dQ Qp CA [NSLICE betaIPx betaIPy sepIP ε_xw ε_yw ε_xs ε_ys]")
        sys.exit(1)
        
    # Add regular emittance if not given in call
    emittNS = ['14','0.4','0.4','0','3e-6','3e-6','3e-6','3e-6']
    sys.argv = sys.argv[:23] + emittNS[NPAR-15:] + sys.argv[23:]
    savedir = "02_pickle_save"

    # HHN_Q340300_X015_A0_N1e5_iT0e0_mT2e4_p-1_nQ101_D311_dQ0_Qp0
    c = 0

    # Piwinski angles     
    gamma = 6500.0/0.938272 
    betaIP = eval(sys.argv[16])
    sigSS = 0.08
    sigTS = np.sqrt(betaIP/gamma*eval(sys.argv[19]))  
        
    bool_multi = 1
    bool_equal = 0

    if eval(sys.argv[2])>1:
        bool_equal = eval(sys.argv[2])>2
        sys.argv[2] = format(float(sys.argv[2])- int(eval(sys.argv[2])) ,'5f')
        bool_multi = 0 
        run = 1
        runNow = 1
        scanType = 2
    for  i,v in enumerate(sys.argv):
        print(i,v)
    print('piwinski',sigSS/sigTS*np.tan(eval(sys.argv[14])*1e-6))
    print('piwinski=1',np.arctan(sigTS/sigSS)*1e6)
    
    man1=man2=man3=man4 = 2
    vals1=[0]
    vals2=[0]
    vals3=[0]
    vals4=[0]
    if bool_multi and scanType<3.5:
    
        man1 = 1    #2 is Qx, 12 = dQ, 15  NSLICE
        man2 = 1    #3 is Qy, 13 = Qp,
        man3 = 2    #4 = XI, 6 = nPart
        man4 = 3    #14 = CA

        vals1 = vals2 = vals3 = vals4 = [0]

        #######################################         
        ############# Tune scans ##############
        #######################################
        if scanType == 1 :
            man3 = 2
            man4 =3         
            sys.argv[man3] = sys.argv[man4] = '0'
               
            # between 4th and 6th
            if job==1:
                vals3 = np.arange(0.255,0.346,0.0025)
                vals4 = vals3

            # Close to half integer
            if job==2:
                vals3 = np.arange(0.45,0.506,0.0025)
                vals4 = vals3
            
            # Close to LHC
            if job ==3:
                vals3 = np.arange(0.305,0.3201,0.001)
                vals3 = np.arange(0.01,0.4901,0.002)
                vals3 = vals3[np.logical_or(vals3<0.305,vals3>0.32)]
                vals4 = vals3
                sys.argv[man4] = '0.01'
        #######################################         
        ######### Big parameter scan ##########
        #######################################         
        XiArr = np.array([0.005,0.015,0.025,0.01,0.02])
        qpArr = np.array([0,15])

        piwArr= np.array([0,0.1,0.3,0.6 ,1   ,1.5,2 ])   # 0, 
        caArr =  np.arctan(piwArr*sigTS/sigSS)*1e6
        betaArr = np.array([0.025, 0.04,0.08,0.12,0.2,0.3,0.8,1.6])
        #        betaArr = np.array([0.025,0.1,1])        


        tuneArr = np.array([0.31, 0.315, 0.32, 0.325, 0.33]) # 0.305 , 0.335
        if scanType == 2 :
            # scan
            # man1 = Q'
            man1 = 13 ; vals1 = qpArr
            sys.argv[man1] = '0'
            
            #man2 = Xi
            man2  = 4 ; vals2 = XiArr # 2 IPS
            sys.argv[man2] = '0'
            
            #man4 = Qx , fixed
            man4 = 2  ; vals4 = [0]
            
            # cross angle scan
            if job==1:
                man3  = 14
                vals3 = caArr
                sys.argv[man3]='0'
                
            # beta scan
            if job==2:
                man3  = 16
                vals3 = betaArr
                sys.argv[man3]='0'
                sys.argv[man3 + 1]='0'

            # beta scan #2 - find best beta!
            if job==3: 
                vals1 = np.array([15])        # Q'
                vals2=vals2[vals2>0.01]         # XI
                man3 = 16
                vals3 = np.arange(0.04,0.2,0.02)
                sys.argv[man3]='0'
                sys.argv[man3 + 1]='0'

            # Noise scan
            if job==4:
                man3  = 1
                vals3 = np.array([0,30,40,50,60])
            
            # Chromaticity scan
            if job==5:
                vals1 = np.arange(0,15.1,2.5)   # Qp
                vals2 =vals2[vals2>0.01]        # XI
                
                man3  = 14                      # CA
                vals3 = caArr[[3]] #0,
                
            # Phase advance scans close to something
            if job ==6:
                vals2 =vals2[vals2>0.01]        # XI
                vals1 = [15]
                man3 = 14                       #CA
                vals3 = np.arctan(np.array([0,1])*sigTS/sigSS)*1e6


                vals1 = [0]
#                vals3 = [0]



                man4  = 12                      #dQ
                vals4 = np.array([-0.03,-0.02,-0.01,-0.005,-0.002,-0.001,-0.0005,0,
                                    0.0005,0.001,0.002,0.005,0.01,0.02,0.03])
                vals4 = np.concatenate(([-0.03,-0.025],np.arange(-0.02,0.0201,0.002),[0.025,0.03]))

            # Phase advance scans across everything
            if job ==7:
                vals2 =vals2[vals2>0.01]        # XI
                vals1 = vals1[vals1==15]        # Q'
                vals2 = vals2[vals2==0.025]
                man3 = 12                       # dQ1
                vals3 = np.arange(0,0.5001,0.0025)
                vals3= vals3[:20]
            
            # Separation scan
            if job ==8:
                vals2 = vals2[vals2>0.01]
                vals1 = [15]
                man3 = 18
                vals3 = [0,1,2,3,4,5,6,7,8,9,10]
                vals3 = [0.25,0.5,0.75,1.25,1.5,1.75]
                
            # Max beam-beam parameters
            if job ==9:
                vals2 = np.arange(0.05,0.101,0.05)
                vals1 = [15]
                man3  = 14                      # CA
                vals3 = caArr[[3]]
                
        # FMA of beta-chroma effect (tune scan)
        if 0:
            man1 = 2 ; vals1 = [0.29,0.30, 0.31, 0.32,0.33]
            man2 = 3 ; vals2 = [0.29,0.30, 0.31, 0.32,0.33]
            man3 = 15
            vals3= np.array([0])
            man4 = 13
            vals4 = qpArr            
        
        
        #######################################         
        ######### Error measurement ##########
        #######################################  
        # Error in emittance growth etc
        if scanType ==3 :
            bool_equal =1
            man1 =6
            vals1 = np.int32(10**np.array([4,4.33,4.67,5,5.33,5.67,6]))
            man2 = man3 = man4 = 2                                                                  
            vals2 = vals3 = vals4 = [0]
            
        
        
        
        # Error in emittance growth
        if 0:
            man2 = man3 = man4 = 2
            vals2 = vals3 = vals4 = [0]

        # get the given values        
        val1Given = eval(sys.argv[man1])
        val2Given = eval(sys.argv[man2])
        val3Given = eval(sys.argv[man3])
        val4Given = eval(sys.argv[man4])
        
        
        
    if bool_equal:
        man4 = 21
        vals4 = np.arange(1,21,1)
        sys.argv = sys.argv + ['0']      
          
    print(sys.argv)
    for iv,v in enumerate([vals1,vals2,vals3,vals4]):
        print(iv+1,v)

    
    runerror = False
    i=0
    if scanType<3.5:
        for val1 in vals1:
            for val2 in vals2:
                for val3 in vals3:
                    for val4 in vals4:
                        if ((val3 <= val4) and (val3<0.4)==(val4<0.4)) or scanType>=1.5:
                            if bool_multi:
                                sys.argv[man1] = format(val1Given+val1,'32e') if man1!=1 else str(val1Given+val1)
                                sys.argv[man2] = format(val2Given+val2,'32e') if man2!=1 else str(val2Given+val2)
                                sys.argv[man3] = format(val3Given+val3,'32e') if man3!=1 else str(val3Given+val3)
                                for im,man in enumerate([man1,man2,man3]):
                                    if man==16:
                                        sys.argv[man+1] = format([val1Given+val1,
                                                val2Given+val2,val3Given+val3][im],'32e')
                                if not bool_equal:
                                    sys.argv[man4] = format(val4Given+val4,'32e')
                            if bool_equal:
                                sys.argv[-1] = str(val4)
                            if scanType ==1 and job==3:
                                if val3!=val4: continue
#                            if scanType <1.5:
#                                if vals3[0] <0.3:
#                                    if val3>=0.295:continue
#                                else:
#                                    if val3>=0.48:continue

                            filename = mods.makeFilename(sys.argv)
                            print(filename +' - '+ str(dt.datetime.now().time())[:8])
                            sys.stdout.flush()
                            sumSim = np.size(vals1)*np.size(vals2)*np.size(vals3)*np.size(vals4)
                
                            i+=1
                            if mods.doit(filename) and run :
                                showprog = sumSim< 4.5 or i < 2
                                c = run_program(sys.argv,filename,bool_showprog=showprog ,bool_plot = sumSim<=2 or True)
        # Extra for NTNU
        pypath = os.environ['PYTHONPATH']
        if mods.doit(filename) and pypath.find('sondrevf')>=0 and run :
            showprog = sumSim< 4.5 or i < 2
            c = run_program(sys.argv,filename,bool_showprog=showprog ,bool_plot = sumSim<=2 or True)
        print(i)
    else:
        if not os.path.isdir(savedir):
            os.makedirs(savedir)
        
        # Jobs are R4,R6,F4,F6    
        sys.argv[15] = ['0','15','0','7'][job-1]
        bool_flat = job>2.5
        
            
        for fill in [5454,5453]:
            fnr = 1
            while os.path.isfile('02_pickle_save/info_simparam%d.txt'%fnr):
                fnr+=1        
            save = False
            #### MD Comparison ####
            with open('11_Distbeam/md_simparam%d.pkl'%fill,'rb') as pklfile:
                [info,Qx_sim, Qy_sim,EXW_sim,EYW_sim,EXS_sim,EYS_sim, XI_sim,XIY_sim,lossRates,growthRates,S_sim,SY_sim] = pkl.load(pklfile)
                    # XI: [ beam i , slot i, step i]
                    # Qs: [ beam i , step i]
                    
    #        for i in range(np.size(Qx_sim)-1,-1,-1):
            for i in range(np.size(Qx_sim)-1,-1,-1):
                sys.argv[2] = format(Qx_sim[i],'32e')
                sys.argv[3] = format(Qy_sim[i],'32e')
                sys.argv[19]= format(EXW_sim[i],'32e')
                sys.argv[20]= format(EYW_sim[i],'32e')
                if bool_flat:
                    fac = SY_sim[i] if int(sys.argv[15]) ==0 else 1
                    sys.argv[4] = format(XIY_sim[i]*fac, '32e')
                    sys.argv[21]= format(EXS_sim[i],'32e')
                    sys.argv[22]= format(EYS_sim[i],'32e')
                else:
                    fac = S_sim[i] if int(sys.argv[15]) ==0 else 1
                    sys.argv[4] = format(XI_sim[i]*fac, '32e')
                    sys.argv[21]= format((EXS_sim[i]+EYS_sim[i])*0.5,'32e')
                    sys.argv[22]= sys.argv[21]
                
                filename = mods.makeFilename(sys.argv)
                
                
                print(info[i], filename +' - '+ str(dt.datetime.now().time())[:8], fac)
                
                sys.stdout.flush()
                if np.isfinite(XI_sim[i]) and (info[i].find("B1 - S2100")>-1 and fill>=5453 ) and (mods.doit(filename) and True) and run:
#                    c = run_program(sys.argv,filename,bool_showprog=(not save),bool_plot = False)
                    info[i] += ' ' + filename
                    save = True
                else:
                    info[i] += ' ' + "norun"

                
            if save:
                np.savetxt('02_pickle_save/info_simparam%d.txt'%fnr,info,fmt='%s')
            else:
                for i,inf in enumerate(info):
                    None
#                    print(inf,"Q %.4f,%.4f | XI %.5f | EW %.3e , %.3e | ES  %.3e , %.3e"%(Qx_sim[i],Qy_sim[i],XI_sim[i],EXW_sim[i],EYW_sim[i],EXS_sim[i],EYS_sim[i]))
                
    return c
if __name__ == "__main__":
    a = main()
    print('errorreturn',a)


#                    nQ = eval(sys.argv[10])
#                    nSnaps = nQ+1
#                    savedir = "02_pickle_save/"+filename
#                    bool1 = False
##                    if  nSnaps > 10:
##                        for i in range(1,nSnaps+1):
##                            if i>2 and (i%max(int(nSnaps/5),1) >0 or nSnaps > 49) and i < nQ+1 :
##                                if os.path.exists(savedir+'/snap_iS%d.p'%(i)):
##                                    os.remove(savedir+ '/snap_iS%d.p'%(i))
##                                    bool1 = True
##                    if bool1: print("Deleted some of:",filename)
