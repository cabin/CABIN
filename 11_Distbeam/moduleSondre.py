#SVF 
#Start 26.08.2016

import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import sys,os
import scipy.constants as const
import scipy.special as spec
import pickle as pkl
from  scipy.stats import norm, chi2
from mpl_toolkits.axes_grid1 import AxesGrid


####################
# Check if it has been simulated before
def doit(filename):
    if os.path.isdir('02_pickle_save/'+filename):
        print('Path already exists, tracking exists')
        return  False
    else:
        return True

####################
def niceNumberString(num):
    if abs(int(num+0.5)-num)<1e-9:
        return '%d'%num
    else:
        return '%.1f'%num

####################
def makeFilename(sysargv):
    filename = ''
    IPS = sysargv[1]
    if float(IPS[0]) and float(sysargv[4])>0:
        filename += 'H'
    else:
        filename += '_'
    if float(IPS[1]):
        filename += 'h'
    if float(IPS[2]) and float(sysargv[4])>0:
        filename += ['V','H'][int(IPS[2]) -1]
    if float(IPS[3]):
        filename += 'v'
    if len(IPS)>4:
        if float(IPS[4]):
            filename+='N'
        if float(IPS[4])>1.5:
            filename+=IPS[4]
    bool_chrom = False
    if len(IPS)>5:
        if float(IPS[5]):
            filename+='C'
            bool_chrom = True
            
    Qx = float(sysargv[2])
    Qy = float(sysargv[3])
    filename += '_Q'+('%.4f'%Qx)[2:]+('%.4f'%Qy)[2:] #str(int(Qx*1000))+str(int(Qy*1000))

    XI = float(sysargv[4])
    filename += '_X'+str(XI)[2:]
    if len(str(XI)[2:]) ==1:
        filename+="0"

    maxamp = float(sysargv[5])
    filename += '_A'+str(int(maxamp))

    N  = int(eval(sysargv[6]))
    if N > 999:
#        filename += '_N%.1fe%d'%(float(str(N)[:2])/10,np.log10(N))
        filename += '_N%se%d'%(str(N)[0],np.log10(N))
    else:
        filename += '_N'+str(int(N))

    # _iT..._mT
    inTurn = int(float(sysargv[7])*10**3)       # num of turns before first measurement
    midTurn = int(float(sysargv[8])*10**3)      # num of turns between tune measurements
    Ti = 0 if inTurn <= 0 else np.log10(inTurn)
    Tm = 0 if midTurn<= 0 else np.log10(midTurn)
    filename += '_iT%de%d_mT%de%d'%(int(str(inTurn)[0]),Ti,int(str(midTurn)[0]),Tm)
   
    ln2turn = int(sys.argv[9])
    nTurn   = int(1024*2**int(round(ln2turn))) # Number of turns = 1024*2**N, this number is N
    n_Qcomp = int(sys.argv[10]) 
    filename += '_p'+str(int(ln2turn))+'_nQ'+str(int(n_Qcomp))
    
    Dist = float(sysargv[11])
    filename += '_D%d'%(Dist)
    
    dQ = float(sysargv[12])
    filename += '_dQ'
    if dQ >= 1:
        filename += 'fh'
    filename += str(dQ)[2:]
    
    Qp = float(eval(sysargv[13])) if bool_chrom else 0
        
    if abs(int(Qp+0.5)-Qp)<1e-9:
        filename += '_Qp%d'%(Qp)
    else:
        filename += '_Qp%.1f'%(Qp)
    
    
    NS = int(eval(sysargv[15]))
    if NS>0:
        CA = int(eval(sysargv[14]))
        filename += '_CA%dur'%CA
        filename += '_NS%d'%NS
    else:
        filename+= '_4D'
        
    betax = eval(sys.argv[16])
    betay = eval(sys.argv[17])
    if betax != 0.4 or betay != 0.4:
        filename += '_b'
        betas = [betax] if betax== betay else [betax,betay]
            
        for beta in betas:
            if abs(int(beta*100+0.5)-beta*100)<1e-9:
                filename += '%dc'%(beta*100)
            else:
                filename += '%.1fc'%(beta*100)

    sepIP = eval(sys.argv[18])
    if sepIP!= 0:
        filename+= '_sIP%.1f'%sepIP

    if len(sys.argv)>22:    
        emitNXW= np.float64(sys.argv[19])           #: Normalized horizontal emittance of weak beam
        emitNYW= np.float64(sys.argv[20])           #: Normalized vertical   emittance of weak beam
        emitNXS= np.float64(sys.argv[21])           #: Normalized horizontal emittance of strong beam
        emitNYS= np.float64(sys.argv[22])           #: Normalized vertical   emittance of strong beam
        standard = 3e-6
        if emitNXW != standard or emitNYW != standard or emitNXS != standard or emitNYS != standard:
            filename+= '_eW%.1f%.1f_eS%.1f%.1f'%(emitNXW*1e6,emitNYW*1e6,emitNXS*1e6,emitNYS*1e6)
    
    if len(sys.argv) > 23:
        filename += '_~%d'%(int(sys.argv[23]))
    
    return filename


####################
def anlzFilename(filename):
    Qs = filename[filename.find('_Q')+2:filename.find('_X')]
    l = int(len(Qs)/2)
    Qx = float(Qs[:l])/10**l
    Qy = float(Qs[l:])/10**l
    XI = float('0.'+filename[filename.find('_X')+2:filename.find('_A')])
    A = float(filename[filename.find('_A')+2:filename[1:].find('_N')+1])
    N = int(eval(filename[filename[1:].find('_N')+3:filename.find('_iT')]))
    nQ = int(filename[filename.find('_nQ')+3:filename.find('_D')])
    p = int(eval(filename[filename.find('_p')+2:filename.find('_nQ')]))
    
    return Qx,Qy,XI,A,N,nQ,int(1024*2**p)

#####################################################################
#####################################################################
def testIC(D,np1d,npGrid):
    if D==1:
        s = np.linspace(-10,10,np1d)
        s[s==0] = 1e-9
        x=xp=y=yp=d = np.zeros_like(s)
    elif D == 2:
        x = np.linspace(-10,10,np1d)
        x[x==0]=1e-9
        xp=y=yp=d=s = np.zeros_like(x)
    elif D==3:
        dx = 10/npGrid
        xamp = np.arange(0.1,10,dx)
        lin = np.linspace(0,10,np1d)
        zer = np.zeros_like(lin)
        x,y,s = np.meshgrid(xamp,xamp,xamp)
        xp = np.zeros_like(x)
        yp = xp.copy()
        d = xp.copy()
        x = np.concatenate((x.reshape((np.size(x),)).astype(np.float64)    ,lin,zer))
        xp= np.concatenate((xp.reshape((np.size(xp),)).astype(np.float64)  ,zer,zer))
        y = np.concatenate((y.reshape((np.size(y),)).astype(np.float64)    ,zer,zer))
        yp= np.concatenate((yp.reshape((np.size(yp),)).astype(np.float64)  ,zer,zer))
        s = np.concatenate((s.reshape((np.size(s),)).astype(np.float64)    ,zer,lin))
        d=  np.concatenate((d.reshape((np.size(d),)).astype(np.float64)    ,zer,zer))
    elif D==4:
        xa = np.linspace(1e-8,10,npGrid)
        x,y = np.meshgrid(xa,xa)
        x = x.reshape((x.size,))
        y = y.reshape((y.size,))
        xp = np.zeros_like(x)
        y  = xp.copy()
        yp = xp.copy()
        s  = xp.copy()
        d  = xp.copy()
         

        
    return x,xp,y,yp,s,d
def initial_distribution(Dist3,nPart,maxA,intensity):
    # Uniform distribution
    bool_yOn = int(str(Dist3)[1])
    bool_prime= int(str(Dist3)[2])
    Dist = int(str(Dist3)[0])
    dim = (1+1*(bool_yOn==1))*(1+1*(bool_prime>0))+2*(Dist>4.5)
    
    if Dist < 4.5:
        sAll = np.random.randn(nPart).astype(np.float64)   # normalized 5th dimension
        dAll = np.random.randn(nPart).astype(np.float64)   # normalized 6th dimension
        
        
    if Dist ==0:
        if 1:
            xAll = np.linspace(1e-9,maxA,nPart)
            yAll = xpAll = ypAll = sAll = dAll = xAll*0
            yAll += 1e-9
            weight = np.ones_like(xAll)*intensity/nPart
            if len(Dist3)>3:
                dAll=np.ones_like(xAll)*int(Dist3[3])
        elif 0:
            s = np.arange(5)
            x = np.linspace(1e-9,maxA,int(nPart/s.size))
            
            xAll,sAll = np.meshgrid(x,s)
            xAll = np.reshape(xAll,(np.size(xAll),))
            sAll = np.reshape(sAll,(np.size(sAll),))
            yAll = xpAll = ypAll = dAll = xAll*0
            yAll = yAll + 1e-10
            weight = np.ones_like(xAll)*intensity/nPart
        else:
            x = np.linspace(1e-9,6,int(nPart/14))
            y = np.array([1e-9,1,2,3,4,5,6])
            X,Y = np.meshgrid(x,y)
            X = np.reshape(X,(X.size,)).astype(np.float64)
            Y = np.reshape(Y,(Y.size,)).astype(np.float64)
            xAll = np.concatenate((X,Y))
            yAll = np.concatenate((Y,X))
            xpAll = ypAll = sAll = dAll = xAll*0
            weight =  np.ones_like(xAll)*intensity/nPart
        
    elif Dist == 1:
        nAmp = np.ceil((nPart)**(1./dim)).astype(int)
        
        bool_dist2 = (sys._getframe(1).f_code.co_name == 'initial_distribution')
        xamp = np.linspace(-maxA*bool_dist2,maxA,nAmp,endpoint=True)
        xamp[xamp == 0] = 1e-5
        if bool_yOn == 1:
            xAll, yAll = np.meshgrid(xamp,xamp)
            xAll = np.reshape(xAll,(np.size(xAll),))
            yAll = np.reshape(yAll,(np.size(yAll),))
        elif bool_yOn == 0:
            xAll = xamp
            yAll = np.ones_like(xAll)*1e-13
        elif bool_yOn == 2:
            xAll = xamp
            yAll = xAll
        if int(Dist3)>=1000:
            sAll *= 0
            dAll *= 0
            dAll += int(str(Dist3)[3])
            if int(Dist3)>=10000:
                dAll -= int(str(Dist3)[4])

#        psAngles = np.zeros_like(xamp) #2*const.pi*np.linspace(0,1,nAmp,endpoint = False)*(bool_dist2) *0
#        print('xmin,xmax', xamp[0],xamp[-1])   
#        xAll  = np.reshape(np.einsum('i,j', xamp,np.cos(psAngles)),(nAmp*np.size(xamp)))
#        yAll  = np.reshape(np.einsum('j,i', xamp,np.cos(psAngles)),(nAmp*np.size(xamp)))
        
        if np.size(xAll) > nPart:
            test = np.sqrt(xAll**2+yAll**2)
            ind = test.argsort()[:-(np.size(xAll)-nPart)]
            xAll = xAll[ind]
            yAll = yAll[ind]
        if bool_yOn ==0 and bool_prime:
            xAll, xpAll = np.meshgrid(xamp,xamp)
            xAll = np.reshape(xAll,(np.size(xAll),))
            xpAll = np.reshape(xpAll,(np.size(xpAll),))
            yAll = np.zeros_like(xAll)
            ypAll = np.zeros_like(yAll)
        elif bool_prime:
            print(np.size(xAll))
            xpAll = xAll[np.argsort(np.random.random(np.size(xAll)))]
            ypAll = yAll[np.argsort(np.random.random(np.size(yAll)))]
        else:
            xpAll = np.zeros_like(xAll)
            ypAll = np.zeros_like(yAll)
        
        weight = np.ones_like(xAll)*intensity/nPart
        
    # Pseudogaussian distribution
    elif Dist == 2:
        xAll, xpAll, yAll, ypAll = initial_distribution(100+10*(bool_yOn>0.5)+bool_prime,nPart,maxA,intensity)[:-3]
        nAmp = nPart**(1/(1+bool_yOn*1))
        dy = 2*maxA/(nAmp-1)
        if bool_yOn ==2: #not bool_prime:
            weight = np.ones_like(xAll)*intensity/nPart
        else:
            weight = intensity*np.exp(-0.5*(xAll**2+xpAll**2+yAll**2+ypAll**2+sAll**2+dAll**2))# * (dy/(2*const.pi)**(1/2))**(dim) #(1+bool_yOn)
            weight = intensity*weight/np.sum(weight)
            if bool_prime:
                weight = weight*nPart
#        weight[xAll*yAll>1e-5] = 2*weight[xAll*yAll>1e-5]

    # Gaussian distribution
    elif Dist == 3:
        yAll = np.ones(nPart).astype(np.float64)*1e-9
        xpAll = np.zeros_like(yAll)
        ypAll = np.zeros_like(yAll)
        if dim == 1:
            xAll = np.random.normal(0,1,nPart)
        elif dim ==4:
            mean = np.zeros(4)
            cov = np.diag(np.ones(4))
            xAll, xpAll, yAll, ypAll = np.random.multivariate_normal(mean,cov,nPart).T
        elif dim ==2:
            mean = np.zeros(2)
            cov = np.diag(np.ones(2))
            if bool_yOn:
                xAll, yAll = np.random.multivariate_normal(mean,cov,nPart).T
            elif bool_prime:
                xAll, xpAll = np.random.multivariate_normal(mean,cov,nPart).T
        weight = np.ones_like(xAll)*intensity/nPart
    # If not 1, 2 or 3
    
    elif Dist == 4:
        nAmp = np.ceil((nPart)**(1./dim)).astype(int)
        
        if nPart > 1e3:
            nAmp = nAmp+(nAmp%2)
        xamp = np.linspace(-maxA,maxA,nAmp,endpoint=True).astype(np.float64)
        
        if np.any(xamp==0):
            xamp[xamp==0] = 1e-13
            print("Some values have been moved from 0 to 1e-9")
        xAll = xamp
#        yAll = np.ones(xamp.size)*1e-13 #(nAmp+(nAmp%2)
        yAll = np.zeros_like(xAll)
        xpAll = np.zeros_like(xAll)
        ypAll = np.zeros_like(xAll)

        if dim == 2:
            if bool_yOn:
                [xAll,yAll] = np.meshgrid(xamp,xamp)
                xpAll = np.zeros_like(xAll)
                ypAll = np.zeros_like(xAll)
            if bool_prime:
                [xAll,xpAll] = np.meshgrid(xamp,xamp)
#                yAll = np.ones(xamp.size**2)*1e-9 #(nAmp+(nAmp%2)
                yAll = np.zeros_like(xAll)
                ypAll = np.zeros_like(xAll)
        elif dim == 4:
            [xAll,yAll,xpAll,ypAll] = np.meshgrid(xamp,xamp,xamp,xamp)
        if bool_yOn == 2:
            yAll = xAll
            ypAll = xpAll

        xAll = xAll.reshape((np.size(xAll),))
        xpAll = xpAll.reshape((np.size(xpAll),))
        yAll = yAll.reshape((np.size(yAll),))
        ypAll = ypAll.reshape((np.size(ypAll),))
        
        if np.size(xAll) > nPart and True:
            test = np.sqrt(xAll**2+xpAll**2 + yAll**2+ypAll**2)
            ind = test.argsort()[:-(np.size(xAll)-nPart)]
            xAll = xAll[ind].copy()
            xpAll = xpAll[ind].copy() 
            
            yAll = yAll[ind].copy()
            ypAll= ypAll[ind].copy()
        

        weight = np.ones_like(xAll)*intensity/nPart
        if bool_yOn<1.5  :
            dy = np.max(np.diff(xamp))
            print(xAll.shape, xpAll.shape,yAll.shape,ypAll.shape)
            weight = intensity*np.exp(-0.5*(xAll**2+xpAll**2+yAll**2+ypAll**2)) * (dy/(2*const.pi)**0.5)**dim
            weight = intensity*weight/np.sum(weight)
    elif Dist == 5:
        yAll = np.ones(nPart).astype(np.float64)*1e-10
        xpAll = np.zeros_like(yAll)
        ypAll = np.zeros_like(yAll)
        sAll = np.zeros_like(yAll)
        dAll = np.zeros_like(yAll)
        
        mean = np.zeros(dim)
        cov = np.diag(np.ones(dim))

        if Dist3 == 500:
            xAll = np.random.randn(nPart)
        if Dist3 == 501:
            if bool_prime:
                xAll, xpAll = np.random.multivariate_normal(np.zeros(2),np.eye(2),nPart).T
        elif dim == 3:
            xAll,sAll,dAll = np.random.multivariate_normal(mean,cov,nPart).T
        elif dim ==4:
            if bool_yOn:
                xAll, yAll,sAll,dAll = np.random.multivariate_normal(mean,cov,nPart).T
            elif bool_prime:
                xAll, xpAll,sAll,dAll = np.random.multivariate_normal(mean,cov,nPart).T
        elif dim == 6:
            xAll,xpAll,yAll,ypAll,sAll,dAll = np.random.multivariate_normal(mean,cov,nPart).T

        weight = np.ones_like(xAll)*intensity/nPart
    elif Dist == 6:
        nAmp = np.ceil((nPart)**(1./6)).astype(int)
        if nPart > 1e3:
            nAmp = nAmp+(nAmp%2)
        xamp = np.linspace(-maxA,maxA,nAmp,endpoint=True).astype(np.float64)
        [xAll,yAll,xpAll,ypAll,sAll,dAll] = np.meshgrid(xamp,xamp,xamp,xamp,xamp,xamp)
        xAll = xAll.reshape((np.size(xAll),))
        xpAll = xpAll.reshape((np.size(xpAll),))
        yAll = yAll.reshape((np.size(yAll),))
        ypAll = ypAll.reshape((np.size(ypAll),))
        sAll = sAll.reshape((np.size(yAll),))
        dAll = dAll.reshape((np.size(ypAll),))

        if np.size(xAll) > nPart and True:
            test = np.sqrt(xAll**2+xpAll**2 + yAll**2+ypAll**2+sAll**2+dAll**2)
            ind = test.argsort()[:-(np.size(xAll)-nPart)]
            xAll = xAll[ind].copy()
            xpAll = xpAll[ind].copy() 
            
            yAll = yAll[ind].copy()
            ypAll= ypAll[ind].copy()

            sAll = sAll[ind].copy()
            dAll = dAll[ind].copy()
            
        # Calc weight
        weight = np.exp(-0.5*(xAll**2+xpAll**2+yAll**2+ypAll**2+sAll**2+dAll**2))*intensity
        weight = intensity*weight/np.sum(weight)
    
    elif Dist == 7:
        xAll = np.random.uniform(-maxA,maxA,size=nPart)
        xpAll= np.random.uniform(-maxA,maxA,size=nPart)
        yAll = np.random.uniform(-maxA,maxA,size=nPart)
        ypAll= np.random.uniform(-maxA,maxA,size=nPart)
        sAll = np.random.uniform(-maxA,maxA,size=nPart)
        dAll = np.random.uniform(-maxA,maxA,size=nPart)
#        test = np.array([xAll**2+yAll**2+sAll**2,xAll**2+xpAll**2,yAll**2+ypAll**2,sAll**2+dAll**2])>maxA**2
        test = np.sqrt(xAll**2+xpAll**2 + yAll**2+ypAll**2+sAll**2+dAll**2)>maxA
        while np.sum(test*1)>0:
#            ind = np.sum(test,axis=0)>0
            ind =test
            for dim in [xAll,xpAll,yAll,ypAll,sAll,dAll]:
                dim[ind] = np.random.uniform(-maxA,maxA,size=np.sum(ind))
#            test = np.array([xAll**2+yAll**2+sAll**2,xAll**2+xpAll**2,yAll**2+ypAll**2,sAll**2+dAll**2])>maxA**2
            test = np.sqrt(xAll**2+xpAll**2 + yAll**2+ypAll**2+sAll**2+dAll**2)>maxA

#        for i in range(4):
#            dim1 = [xAll,xAll,yAll,sAll][i]
#            dim2 = [yAll,xpAll,ypAll,dAll][i]
#            while np.sum(dim1**2+dim2**2 > maxA**2)>0:
#                ind = dim1**2+dim2**2 > maxA**2
#                dim1[ind] = np.random.uniform(-maxA,maxA,size=np.sum(ind))
#                dim2[ind] = np.random.uniform(-maxA,maxA,size=np.sum(ind))

        # Calc weight
        weight = np.exp(-0.5*(xAll**2+xpAll**2+yAll**2+ypAll**2+sAll**2+dAll**2))*intensity
        weight = intensity*weight/np.sum(weight)
    
    elif Dist == 8:
        def stepwisenormal(N,lims):
            dim=2
            limN=np.linspace(0,N,np.size(lims)).astype(np.int)
            # weightZones = 2*((norm.cdf(lims[1:])-0.5)**dim-(norm.cdf(lims[:-1])-0.5)**dim)
            weightZones = 2*(np.exp(-lims[:-1]**2/2)-np.exp(-lims[1:]**2/2))*N/np.diff(limN)

            n=np.size(lims)-1
            a = np.ones(N)
            b = np.ones(N)
            weight = np.ones(N)

            trymax= 5e4
            for i in range(n):
                ll=lims[i] ; hl=lims[i+1]
                ul=np.exp(-ll**2/2) ; uh=np.exp(-hl**2/2)
                ai = a[limN[i]:limN[i+1]]
                bi = b[limN[i]:limN[i+1]]
                print(ll,hl,ai.size,bi.size)
                test = ai >-1
                count=0
                while np.sum(test)>0 and count<trymax:
                    Ni = np.sum(test)
                    XX = np.sqrt(-2*np.log(np.random.uniform(uh,ul,Ni)))
                    phi = np.random.uniform(0,2*np.pi,Ni)
                    ai[test] = XX*np.cos(phi)
                    bi[test] = XX*np.sin(phi)
        #             ai[test],bi[test]  = np.random.multivariate_normal(np.zeros(dim),np.eye(dim),Ni).T
                    rs = np.sqrt(ai**2+bi**2)
                    test = np.logical_or(rs<ll,rs>hl)
                    if count%(trymax/10)==0: print(np.sum(test),ai.size,test.size,rs.size)
                    count+=1
                print(np.sum(test))
                a[limN[i]:limN[i+1]] = ai
                b[limN[i]:limN[i+1]] = bi
                weight[limN[i]:limN[i+1]] = weightZones[i]
            
            return a,b,weight
        def stepwisenormal4D(N,lims):
            dim = 4
            Ns = np.linspace(0,N,np.size(lims)).astype(np.int)
            nZones=np.size(lims)-1
            weightZones = np.zeros(nZones)
            xAll,xpAll,yAll,ypAll = np.random.multivariate_normal(np.zeros(dim),np.eye(dim),N).T
            weight = np.zeros_like(xAll)
            
            for i in range(nZones):
                N1=Ns[i]   ; N2=Ns[i+1]
                ll=lims[i] ; hl=lims[i+1]
                
                R = np.sqrt(xAll**2+xpAll**2 + yAll**2+ypAll**2)[N1:N2]
                ind = np.logical_or(R<=ll, R>hl)
                test=np.sum(ind)
                count=0
                while test:
                    NS = np.random.multivariate_normal(np.zeros(dim),np.eye(dim),test)
                    if test<1000:
                        NS1 = np.random.multivariate_normal(np.zeros(dim),np.eye(dim),100)
                        NS2 = np.sum(NS1**2,axis=1)
                        ind2 = np.logical_and(NS2>=ll**2,NS2<=hl**2)
                        NS[:min(np.sum(ind2),test)] = NS1[ind2,:][:min(np.sum(ind2),test)]

                    xAll[N1:N2][ind],xpAll[N1:N2][ind],yAll[N1:N2][ind],ypAll[N1:N2][ind] = NS.T
                    
                    R = np.sqrt(xAll**2+xpAll**2 + yAll**2+ypAll**2)[N1:N2]
                    ind = np.logical_or(R<=ll, R>hl)
                    test = np.sum(ind>0)
                    
                    if count%1e3==0:
                        print(test,'left')
                    count+=1
                weight[N1:N2] = (chi2.cdf(x=hl**2, df=4)-chi2.cdf(x=ll**2,df=4))*N/(N2-N1)
            
            return xAll,xpAll,yAll,ypAll,weight
            
        if not bool_prime:        
            xAll,xpAll,yAll,ypAll,sAll,dAll=np.zeros((6,nPart))#.astype(np.float64)
            d2s =[xAll,xpAll,yAll,ypAll,sAll,dAll]
            for di in d2s:
                di[:] = np.random.normal(0,1,nPart)

            lims = np.array([0,4,np.inf])
            xAll,yAll,weight1=stepwisenormal(nPart,lims)

    #        yAll,ypAll,weight2=stepwisenormal(nPart,lims)
    #        yAll = np.roll(yAll,int(nPart/4))
    #        ypAll=np.roll(ypAll,int(nPart/4))
    #        weight2=np.roll(weight2,int(nPart/4))
            weight=weight1 #weight2
            
        else:
            lims= [0,2,np.inf]
            xAll,xpAll,yAll,ypAll,weight = stepwisenormal4D(nPart,lims)
            sAll = np.random.normal(0,1,nPart)
            dAll = np.random.normal(0,1,nPart)

        weight = intensity*weight/np.sum(weight)

    elif Dist == 9 and int(Dist3)<1000:
        xAll = np.random.uniform(-maxA,maxA,size=nPart)
        xpAll= np.random.uniform(-maxA,maxA,size=nPart)
        yAll = np.random.uniform(-maxA,maxA,size=nPart)
        ypAll= np.random.uniform(-maxA,maxA,size=nPart)
        sAll = np.random.uniform(-maxA,maxA,size=nPart)
        dAll = np.random.uniform(-maxA,maxA,size=nPart)
        weight =np.zeros_like(xAll)
        
        reg = 3
        dim=6
        Ns = np.linspace(0,nPart,reg+1).astype(np.int32)
#        Ns = np.array([0,10000,50000,nPart])
        As = np.linspace(0,maxA,reg+1)
        weightfac = (As[1:]**dim-As[:-1]**dim)*nPart/np.diff(Ns)
        print(Ns,As,weightfac)
        for i in range(As.size-1):
            ll=As[i] ; hl=As[i+1]
            N1=Ns[i] ; N2=Ns[i+1]

            print(ll,hl,N1,N2)            
#            R2 = np.sqrt([xAll**2+yAll**2+sAll**2,xAll**2+xpAll**2,yAll**2+ypAll**2,sAll**2+dAll**2])
#            test = np.sum(np.logical_or(R2<=ll,R2>hl),axis=0)>0
            R2 = np.sqrt(xAll**2+xpAll**2 + yAll**2+ypAll**2+sAll**2+dAll**2)[N1:N2]
            test = np.logical_or(R2<=ll,R2>hl)
            while np.sum(test)>0:
                for dim in [xAll,xpAll,yAll,ypAll,sAll,dAll]:
                    dim[N1:N2][test] = np.random.uniform(-hl,hl,size=np.sum(test))
                R2 = np.sqrt(xAll**2+xpAll**2 + yAll**2+ypAll**2+sAll**2+dAll**2)[N1:N2]
                test = np.logical_or(R2<=ll,R2>hl) #np.sum(np.logical_or(R2<=ll,R2>hl),axis=0)>0
            # Calc weight
            weight[N1:N2] = np.exp(-0.5*(xAll**2+xpAll**2+yAll**2+ypAll**2+sAll**2+dAll**2))[N1:N2]*weightfac[i]
        weight = intensity*weight/np.sum(weight)        
    elif Dist==9:
        xAll = np.random.uniform(-maxA,maxA,size=nPart)
        xpAll= np.random.uniform(-maxA,maxA,size=nPart)
        yAll = np.random.uniform(-maxA,maxA,size=nPart)
        ypAll= np.random.uniform(-maxA,maxA,size=nPart)
        sAll = np.random.uniform(-maxA,maxA,size=nPart)
        dAll = np.random.uniform(-maxA,maxA,size=nPart)
        weight =np.zeros_like(xAll)
        
        reg = 3
        dim=6
        Ns = np.linspace(0,nPart,reg+1).astype(np.int32)

        As = np.linspace(0,maxA,reg+1)
        
        #weighting due to how many particles represent a volume
        weightfac = (As[1:]**dim-As[:-1]**dim)/As[-1]**dim*nPart/np.diff(Ns)
        
        # Weighting due to how much weight should be in a region
#        weightZones =2*(np.exp(-As[:-1]**2/2)-np.exp(-As[1:]**2/2))*np.diff(Ns)/nPart
        weightZones = (chi2.cdf(x=As[1:]**2, df=6)-chi2.cdf(x=As[:-1]**2,df=6))*np.diff(Ns)/nPart
        print(weightfac,weightZones)
        print(Ns,As,weightfac)
        for i in range(As.size-1):
            ll=As[i] ; hl=As[i+1]
            N1=Ns[i] ; N2=Ns[i+1]

            print(ll,hl,N1,N2)            
            R2 = np.sqrt(xAll**2+xpAll**2 + yAll**2+ypAll**2+sAll**2+dAll**2)[N1:N2]
            test = np.logical_or(R2<=ll,R2>hl)
            while np.sum(test)>0:
                if i ==0:
                    for dim in [xAll,xpAll,yAll,ypAll,sAll,dAll]:
                        dim[N1:N2][test] = np.random.randn(np.sum(test))
                else:
                    for dim in [xAll,xpAll,yAll,ypAll,sAll,dAll]:
                        dim[N1:N2][test] = np.random.uniform(-hl,hl,size=np.sum(test))
                R2 = np.sqrt(xAll**2+xpAll**2 + yAll**2+ypAll**2+sAll**2+dAll**2)[N1:N2]
                test = np.logical_or(R2<=ll,R2>hl) #np.sum(np.logical_or(R2<=ll,R2>hl),axis=0)>0
            # Calc weight
            if i>0:
                weight[N1:N2] = np.exp(-0.5*(xAll**2+xpAll**2+yAll**2+ypAll**2+sAll**2+dAll**2))[N1:N2]*weightfac[i]
            else:
                weight[N1:N2] = (N2-N1)/nPart*weightfac[i]*weightZones[i]/np.sum(weightZones)
        totweight = np.sum(weight)
        for  i in range(reg):
            weight[Ns[i]:Ns[i+1]] *= weightZones[i]*totweight /np.sum(weight[Ns[i]:Ns[i+1]])

        weight = intensity*weight/np.sum(weight)   


    else:
        print("Selected distribution out of range, the program will stop")
        sys.exit(1)


    print("%15s%.3e"%('Sum(weights) = ',np.sum(weight)))  #Should return intensity
    return np.copy(xAll), np.copy(xpAll), np.copy(yAll), np.copy(ypAll), np.copy(sAll), np.copy(dAll), np.copy(weight)

#####################
def noOutput(start, save = 1,null_fds=1):
    if start:
        null_fds = [os.open(os.devnull, os.O_RDWR) for x in range(2)]
        save = os.dup(1), os.dup(2)
        os.dup2(null_fds[0], 1)
        os.dup2(null_fds[1], 2)
        return save,null_fds
    else:
        # restore file descriptors so I can print the results
        os.dup2(save[0], 1)
        os.dup2(save[1], 2)
        # close the temporary fds
        os.close(null_fds[0])
        os.close(null_fds[1]) 
        return save,null_fds        

##########################################################################################
####################################### GPU-KERNEL #######################################
########################################################################################## 
def meminfo(kernel):
    shared=kernel.shared_size_bytes
    regs=kernel.num_regs
    local=kernel.local_size_bytes
    const=kernel.const_size_bytes
    mbpt=kernel.max_threads_per_block
    print("""=MEM=\nLocal:%d,\nShared:%d,\nRegisters:%d,\nConst:%d,\nMax Threads/B:%d"""%(local,shared,regs,const,mbpt))
    return mbpt
    
     
def cstr(arr):
    return "{" + ','.join([str(x) for x in arr]) + "}"     


def STSLD(sigSS=1,sigDS=1,betaXS=1,emitXS=1,betaYS=1,emitYS=1,nSlice=1,crossAS=140e-6):
    if nSlice ==0: nSlice=1
    sliceX = np.zeros((nSlice),dtype=np.float64)
    sliceY = sliceX.copy()
    sliceS = sliceX.copy()
    
    
    bord1 = -10
    for i in range(nSlice):
        relZ = 1/nSlice*(i+1)
        
        if i == nSlice-1:
            bord2 = 10
        else:
            bord2 = norm.ppf(relZ)
        
        # Set value of X,Y,Z of slices of strong beam in horizontal crossing
        sliceS[i] = nSlice/np.sqrt(2*np.pi)*(np.exp(-bord2**2/2)-np.exp(-bord1**2/2))*sigSS/np.cos(crossAS)
        sliceX[i] = sliceS[i]*np.sin(crossAS)
        sliceY[i] = 0
        
        bord1 = bord2

    # Beamsize at s* = 0 in head-on frame
    sigXXS_HO = emitXS*betaXS
    sigYYS_HO = emitYS*betaYS
    sigPPS_HO = emitXS/betaXS/np.cos(crossAS)**2
    sigQQS_HO = emitYS/betaYS/np.cos(crossAS)**2
    
    return sliceX,sliceY,sliceS, sigXXS_HO, sigYYS_HO, sigPPS_HO, sigQQS_HO
    
##########################################################################################
#################################### POST-PROCESSSING ####################################
##########################################################################################
def get_lossrate(turns,intensity,freq,t1 =5e5):
    fit = np.polyfit((turns[turns>t1]-t1)/(3600*freq),intensity[turns>t1]/1e11,deg=1)
    return -fit[0]/fit[1]

def get_dynAper(savedir):
# Dynamic aperture is lowest initial amplitude in x,y (in sigmas of strong beam) for particle that is lost beyond the machine limit
    lim = 2.5
    dynapfile = savedir+ '/dynApB%.1f.txt'%lim
    
    if (os.path.isfile(dynapfile)) and 1:
        return np.loadtxt(dynapfile)
    else:
        # Import parameters.p
        with open(savedir+'/parameters.p','rb') as pklfile:
            parameters = pkl.load(pklfile)  
            [ IPS,Qx0,Qy0,Qx1,Qy1,XI,intensity,gamma,crossAS,betaIP, 
        emitNXW,emitNYW,emitNXS,emitNYS,Qs,Qs1, NR_G, lost_beam,lost_mach,
        headonC, nLR, longrangeC, normsepLR, noiseA,sigSW,sigDW,Qp,Qp1, maxA,nPart,inTurn,midTurn,ln2turn,
        nSave,nMean,nQ,Dist,nGroup,indGroup,turnArr,bool_savePlotPkl,bool_tuneArr,bool_tune ] = parameters
                    
        with open(savedir + '/snap_iS1.p','rb') as pklfile:
            snapfile = pkl.load(pklfile)  #, encoding='latin1'
            [iTurn,xAll,xpAll,yAll,ypAll,sAll,dAll,weight] = snapfile[:8]
        with open(savedir + '/snap_iS%d.p'%(nQ+1),'rb') as pklfile:
            snapfile = pkl.load(pklfile)  #, encoding='latin1'
            [inBeamAll,inMachAll] = snapfile[8:10]
            inBeamAll = inBeamAll.astype(np.bool)
            inMachAll = inMachAll.astype(np.bool)

        mask = np.logical_and(np.logical_not(inBeamAll),(sAll**2+dAll**2)<lim**2)

        if not mask.any():
            dynap = np.infty
        else:
            dynap = np.sqrt(np.min(((xAll**2+xpAll**2)*(emitNXW/emitNXS)+
                                        (yAll**2+ypAll**2)*(emitNYW/emitNYS))[mask]))
        np.savetxt(dynapfile,[dynap])
        return dynap
        
def get_piwAngle(savedir, horCross = True):
    # Import parameters.p
    with open(savedir+'/parameters.p','rb') as pklfile:
        parameters = pkl.load(pklfile)  
        [IPS,Qx0,Qy0,Qx1,Qy1,XI,intensity,gamma,crossAS,betaIP, 
            emitNXW,emitNYW,emitNXS,emitNYS,Qs,Qs1, NR_G, lost_beam,lost_mach,
            headonC, nLR, longrangeC, normsepLR, noiseA,sigSW,sigDW,Qp,Qp1, maxA,nPart,inTurn,midTurn,ln2turn,
            nSave,nMean,nQ,Dist,nGroup,indGroup,turnArr,bool_savePlotPkl,bool_tuneArr,bool_tune] = parameters
        sigSS = sigSW
    if horCross:
        angle = sigSS/np.sqrt(emitNXS*betaIP/gamma)*np.tan(crossAS)
    else:
        angle = sigSS/np.sqrt(emitNYS*betaIP/gamma)*np.tan(crossAS)
    return  int(100*angle+0.5)/100.0
##########################################################################################
######################################## PLOTTING ########################################
##########################################################################################
def initialize(seePlots=True):
    matplotlib.interactive(True)
    # Set size of plots  
    ticksize= 17  
    titlesize = 25  
    matplotlib.rcParams.update({'font.size':ticksize }) # legend  
    matplotlib.rcParams.update({'ytick.labelsize':ticksize })  
    matplotlib.rcParams.update({'xtick.labelsize':ticksize})  
    matplotlib.rcParams.update({'axes.labelsize':titlesize-2})    # x,y,cbar labels
    matplotlib.rcParams.update({'figure.titlesize' :titlesize})  
    matplotlib.rcParams.update({'axes.titlesize':titlesize})       # Title
    matplotlib.rcParams.update({'savefig.bbox':'tight'})  
    matplotlib.rcParams.update({'axes.formatter.limits':[-4,4]})  
    matplotlib.rcParams['figure.figsize'] = plotsize()
    matplotlib.interactive(seePlots) 

####################
def plotsize(add1=0,add2=0):
    return 8+add1,7+add2

####################
def colorLimits():
    vmin = -14   + 5  *0
    vmax = -7    - 1  *0
    return vmin,vmax
    
####################
def modFontSize():
    return 24

#####################
def maintitle(fig,filename,size =10):
    fig.subplots_adjust(left = 0.08,right = 0.92,top = 0.86) #,wspace = 0.2)
    
    maintitle = filename # filename[:filename.find('_N')]
    axtemp = fig.add_subplot(111,alpha =0,aspect='equal')
    sup1 = axtemp.set_title(maintitle+"\n",y=1.14,fontsize = size)
    axtemp.axis('off')
    return sup1
    
    
#####################
def shiftedColorMap(cmap, start=0, midpoint=0.5, stop=1.0, name='shiftedcmap'):
    '''
    Function to offset the "center" of a colormap. Useful for
    data with a negative min and positive max and you want the
    middle of the colormap's dynamic range to be at zero

    Input
    -----
      cmap : The matplotlib colormap to be altered
      start : Offset from lowest point in the colormap's range.
          Defaults to 0.0 (no lower ofset). Should be between
          0.0 and `midpoint`.
      midpoint : The new center of the colormap. Defaults to 
          0.5 (no shift). Should be between 0.0 and 1.0. In
          general, this should be  1 - vmax/(vmax + abs(vmin))
          For example if your data range from -15.0 to +5.0 and
          you want the center of the colormap at 0.0, `midpoint`
          should be set to  1 - 5/(5 + 15)) or 0.75
      stop : Offset from highets point in the colormap's range.
          Defaults to 1.0 (no upper ofset). Should be between
          `midpoint` and 1.0.
    '''
    cdict = {
        'red': [],
        'green': [],
        'blue': [],
        'alpha': []
    }

    # regular index to compute the colors
    reg_index = np.linspace(start, stop, 257)

    # shifted index to match the data
    shift_index = np.hstack([
        np.linspace(0.0, midpoint, 128, endpoint=False), 
        np.linspace(midpoint, 1.0, 129, endpoint=True)
    ])

    for ri, si in zip(reg_index, shift_index):
        r, g, b, a = cmap(ri)

        cdict['red'].append((si, r, r))
        cdict['green'].append((si, g, g))
        cdict['blue'].append((si, b, b))
        cdict['alpha'].append((si, a, a))

    newcmap = matplotlib.colors.LinearSegmentedColormap(name, cdict)
    plt.register_cmap(cmap=newcmap)

    return newcmap

#################### 
# Plot tunemap, colored by diffusion
def freqmapDiff(filename, Qx1,Qy1,colorsource,fignr=1, subplt=111,bool_cb = True,bool_save = False,prefix = 'fma_fd',app=""):
    save,files = noOutput(True)
    Qx,Qy,XI,A,N,nQ,nTurn = anlzFilename(filename)
    vmin,vmax = colorLimits()
    # Axes Axis
    fig = plt.figure(fignr)
    ax = subplt
    DQ = max(Qx1)-min(Qx1) ; r = 0.02
    if type(subplt) == type(1):
        ax = fig.add_subplot(subplt,aspect = 'equal')
        lims = [min(Qx1)-r*DQ,max(max(Qx1),Qx)+r*DQ,min(Qy1)-r*DQ,max(max(Qy1),Qy)+r*DQ]
    else:
        lims = np.append(ax.get_xlim(),ax.get_ylim())
        lims = [min(min(Qx1)-r*DQ,lims[0]),max(max(max(Qx1),Qx)+r*DQ,lims[1])
                ,min(min(Qy1)-r*DQ,lims[2]),max(max(max(Qy1),Qy)+r*DQ,lims[3])]
    #Plot
    cax = ax.scatter(Qx1,Qy1,c=colorsource,cmap='jet',edgecolor='none',s=7,vmin = vmin, vmax =vmax)
    if bool_cb:
        cbar = plt.colorbar(cax,ax=ax,ticks = np.arange(vmin,vmax+0.5), label = ('Diffusion coefficient'),fraction=0.046, pad=0.04) 
    ax.plot(Qx,Qy,c='r',marker='x',ms=10,mew = 2)
    ax.axis(lims)

    ax.set_title("Tunemap\n")
    ax.set_xlabel(r"$Q_x$")
    ax.set_ylabel(r"$Q_y$")
#    ax.axis([Qx-0.031,Qx+0.001,Qy-0.031,Qy+0.001])
    fig.tight_layout()
    
    #Maintitle
    sup1 = maintitle(fig,filename)
    
    #save
    if bool_save:
        dumpname = "01_Plots/"+prefix+'_'+filename+app+".png"
        plt.savefig(dumpname, bbox_inches='tight',bbox_extra_artists=[sup1])
    
    noOutput(False,save,files)
    return ax

###############################################################################
# Plot tunemap, colored by amplitude
# alternatives cmaps: jet, CMRmap
def freqmapAmp(filename, Qx1,Qy1,colorsource,fignr=1, subplt=111,bool_cb = True,bool_save = False,prefix = 'fma_fa'):
    save,files = noOutput(True)
    Qx,Qy,XI,A,N,nQ,nTurn = anlzFilename(filename)
    
    # Axes Axis
    fig = plt.figure(fignr)
    ax = subplt
    DQ = max(Qx1)-min(Qx1) ; r = 0.02
    if type(subplt) == type(1):
        ax = fig.add_subplot(subplt,aspect = 'equal')
        lims = [min(Qx1)-r*DQ,max(max(Qx1),Qx)+r*DQ,min(Qy1)-r*DQ,max(max(Qy1),Qy)+r*DQ]
    else:
        lims = np.append(ax.get_xlim(),ax.get_ylim())
        lims = [min(min(Qx1)-r*DQ,lims[0]),max(max(max(Qx1),Qx)+r*DQ,lims[1]),min(min(Qy1)-r*DQ,lims[2]),max(max(max(Qy1),Qy)+r*DQ,lims[3])]

    #Plot
    cax = ax.scatter(Qx1,Qy1,c=np.floor(colorsource-1e-7),cmap='jet',edgecolor='none',s=8,vmin=0,vmax=min(14,max(colorsource)))
    if bool_cb:
        cbar = ax.colorbar(cax,ax=ax,boundaries = np.arange(0,max(colorsource)+0.1,1),ticks= np.arange(0,max(colorsource)+0.5,2),
                            spacing = 'uniform', label=r'Ampltiude, $A_x$',fraction=0.046, pad=0.04)
    ax.plot(Qx,Qy,c='r',marker='x',ms=10,mew = 2)
    ax.axis(lims)

    ax.set_xlabel(r"$Q_x$")
    ax.set_ylabel(r"$Q_y$")
    ax.set_title("Tunemap\n")
    fig.tight_layout()
    
    #Maintitle
    sup1 = maintitle(fig,filename)

    #save
    if bool_save:
        dumpname ="01_Plots/"+prefix+'_' + filename+".png"
        plt.savefig(dumpname, bbox_inches='tight',bbox_extra_artists=[sup1])
    noOutput(False,save,files)
    return ax
###############################################################################
def ampDiff(filename, RX, RY,colorsource,fignr=1, subplt=111,bool_cb = True,bool_save = False, prefix = 'fma_ad' ,app=""):
    save,files = noOutput(True)
    Qx,Qy,XI,A,N,nQ ,nTurn= anlzFilename(filename)
    vmin,vmax = colorLimits()
    
    # Axes Axis
    fig = plt.figure(fignr)
    ax = subplt
    if type(subplt) == type(1):
        ax = fig.add_subplot(subplt,aspect = 'equal')
        lims = [min(RX),max(RX),min(RY),max(RY)]
    else:
        lims = np.append(ax.get_xlim(),ax.get_ylim())
        lims = [min(min(RX),lims[0]),max(max(RX),lims[1]),min(min(RY),lims[2]),max(max(RY),lims[3])]
    #Plot
    dist5 = int(filename[filename.find('_D')+2]) == 5    
    dist3 = int(filename[filename.find('_D')+2]) == 3
    dist1 = int(filename[filename.find('_D')+2]) == 1
    size = (fig.get_size_inches()[1]*fig.dpi)**2*2/N*1.5*(1-0.8*(dist3+dist5))/(1+3*dist1)
    cax = ax.scatter(RX,RY,c=colorsource,cmap='jet',edgecolor='none',vmin=vmin,vmax=vmax,s = size)
    ax.axis(lims)
    if bool_cb:
        cbar = plt.colorbar(cax,ax=ax,ticks = np.arange(vmin,vmax+0.5),label =('Diffusion coefficient'),fraction=0.046, pad=0.04)
    ax.set_title("Amplitude map\n",)
    ax.set_xlabel(r'$A_x \, [\sigma_{_\Sigma}]$')
    ax.set_ylabel(r'$A_y \, [\sigma_{_\Sigma}]$')
    fig.tight_layout()

    #Maintitle
    sup1 = maintitle(fig,filename)

    #save
    if bool_save:
        dumpname = "01_Plots/"+prefix+'_' + filename+".png" 
        plt.savefig(dumpname, bbox_inches='tight',bbox_extra_artists=[sup1])

    noOutput(False,save,files)
    return ax
    
    
#    sup1 = ax.set_title('123')
#    sup1 = fig.suptitle(maintitle,y=0.5,fontsize = modFontSize(),transform = ax.transAxes)    
    
#    if len(fig.axes) == 1+bool_cb*1:
#        sup1 = plt.text(0.5,1.08,maintitle,horizontalalignment='center',fontsize = modFontSize(),transform = ax.transAxes)
#    elif type(subplt) == type(1):
#        sup1= fig.suptitle(maintitle,y=1.0,fontsize = modFontSize())

