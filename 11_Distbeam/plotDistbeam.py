#SVF 
#Start 14.09.2016

import sys,os
import numpy as np
import time 
from scipy.stats import norm
import scipy.constants as const
import matplotlib
matplotlib.use('Agg')
import matplotlib.ticker as ticker
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import pickle as pkl
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.colors import LogNorm
import datetime as dt
#Import private scripts
if os.path.exists('11_Distbeam'):
    sys.path.append('11_Distbeam')
import moduleSondre as mods
from moduleSondre import noOutput
mods.initialize()

#############################################################
#############################################################
#############################################################
def main():
    print('\nplotDistbeam.py    - '+str(dt.datetime.now().time())[:8])
    print('sys.argv = [pickle_folder, PLOTTING_Decisions, snapsToPlot, tlim_depsdt]')
    sys.stdout.flush()
    mods.initialize()

    ############################    
    #### Running parameters ####
    
    # What plots to plot
    showplots  = 0                  #: If show plots, instead of just saving them
    plot_snaps = 1                  #: True: Plot multiple snapshots
    plot_depsdt = 1                 #: True: Plot time evolution of beamemittance and luminosity
    plot_singleplots = 0            #: True: Plot 
    bool_stopstdout = 0             #: True: Stop output from going to the terminal
    if np.size(sys.argv)>2:
        bools = sys.argv[2]
        showplots = float(bools[0])
        plot_snaps = float(bools[1])
        plot_depsdt = float(bools[2])
        plot_singleplots = float(bools[3])
    plt.interactive(showplots)
        
    # Check that the given directories exists
    savedir = '02_pickle_save/'+sys.argv[1]
    if not os.path.exists(savedir):
        print('Given directory does not exists: %s'%savedir)
        sys.exit()
        
    plotdir = '01_Plots'
    if not os.path.exists(plotdir):
        os.makedirs(plotdir)

    # Import parameters.p
    with open(savedir+'/parameters.p','rb') as pklfile:
        parameters = pkl.load(pklfile)  
        [IPS,Qx0,Qy0,Qx1,Qy1,XI,intensity,gamma,crossAS,betaIP, 
            emitNXW,emitNYW,emitNXS,emitNYS,Qs,Qs1, NR_G, lost_beam,lost_mach,
            headonC, nLR, longrangeC, normsepLR, noiseA,sigSW,sigDW,Qp,Qp1, maxA,nPart,inTurn,midTurn,ln2turn,
            nSave,nMean,nQ,Dist,nGroup,indGroup,turnArr,bool_savePlotPkl,bool_tuneArr,bool_tune] = parameters
 
    # Do not plot a small set of particles
    if nPart <1000 and int(Dist/100)==3:
        sys.exit(1)
 
    # Do not plot small amount of particles
    if nPart<100 and True:
        print("To few particles to plot")
        sys.exit(0)
        
    # Import time evolutions of different properties
    if os.path.isfile(savedir +'/beamEvol.p'):
        with open(savedir +'/beamEvol.p','rb') as pklfile:
            [turns, emittX,emittY, emittX6, emittY6,beamIntensity,beamSize,beamLuminosity,
                                                 machIntensity,machSize,machLuminosity,emittZ] = pkl.load(pklfile) 


    #############################################
    ############### SNAP PLOTS ##################
    maps = ['hot_r','jet']

    nSnaps = np.size(turnArr)                                                     #: # of snapfiles that should exist
    nSnapstoPlot = min(2,nSnaps) if plot_snaps else 1                             #: # of snapshots to plot
    snapsToPlot = np.linspace(1,np.size(turnArr)-1,nSnapstoPlot).astype(np.int32) #: Which snaps to plot
    if len(sys.argv)>3:
        snapsToPlot = np.append(snapsToPlot,int(sys.argv[3]))
    print(snapsToPlot)
    iSnap = 1    
    for iSnap in snapsToPlot:
        if (os.path.isfile(savedir + '/snap_iS%d.p'%iSnap)) and plot_snaps:
            # Load snapfile
            with open(savedir + '/snap_iS%d.p'%iSnap,'rb') as pklfile:
                abc = pkl.load(pklfile)  #, encoding='latin1'
                [iTurn,xAll,xpAll,yAll,ypAll,sAll,dAll,weight,inBeamAll,inMachAll,iQ,
                    QxAll,QyAll,jxAll,jyAll,smearxAll,smearyAll,jzAll] = abc
                inBeamAll = inBeamAll.astype(np.bool)
                inMachAll = inMachAll.astype(np.bool)
                
#            print(iSnap,iTurn,iQ, np.sum(QxAll == 0), np.sum(QyAll == 0))

            ###########################################################         
            # Plot the different distributions to see if they look good
            if iSnap == 1:
                vmin = 1e-1 
                top = max(6,maxA*( not(299<Dist<400) and not (500<=Dist<600)))
                nbins = (1+np.log(nPart)/np.log(2))*top/2
                edges = np.linspace(-top,top,nbins)
                edges1D = np.linspace(-top,top,nbins)

                edgesAct = np.linspace(0,top**2/2,nbins)  
                
                # prepare 1D plots
                dx = np.diff(edges1D[0:2])                
                center1D = edges1D[:-1]+dx/2
                x = np.linspace(-top,top,100)
                gauss = norm.pdf(x,0,1)*intensity
                gaussBins = norm.pdf(center1D,0,1)*intensity
                gaussPeak = norm.pdf(0,0,1)*intensity
                ticksize = 0.05      
                initX = np.histogram(xAll,bins = edges1D,weights=weight/dx)[0]
                initXP= np.histogram(xpAll,bins = edges1D,weights=weight/dx)[0]
                initY = np.histogram(yAll,bins = edges1D,weights=weight/dx)[0]
                initYP= np.histogram(ypAll,bins = edges1D,weights=weight/dx)[0]                      
            
            # Plot if tune is measured 
            # Plot if specifically told so
            if (iQ>0  and plot_snaps) or np.any(snapsToPlot[:] == iSnap) :
                # Stop warnings from matplotlib
                if bool_stopstdout:           
                    save,null_fds = noOutput(True) 
                if int(Dist/100)==3 or int(Dist/100)==5 :
                    vmin = 10**int(np.log10(np.min(weight)))
                
                fig = plt.figure(figsize=(34,14))

                # Plot tune Histogram or scatter distribution                
                if (np.size(QxAll)>1):
                    if Dist%100<10:
                        ax1 = fig.add_subplot(251)
                    else:
                        ax1 = fig.add_subplot(251,aspect = 'equal')
                    ax1.set_title(r'Histogram of $(Q_x,Q_y)$')
                    ax1.set_xlabel(r'$Q_x$') ; ax1.set_ylabel(r'$Q_y$')
                    
                    ind = np.isfinite(QxAll+QyAll)
                    plt.hist2d(QxAll[ind],QyAll[ind],weights = weight[ind],bins = nbins*2,cmap = maps[1],norm=LogNorm(),vmin=vmin)
                    ax1.plot(Qx0,Qy0,c='r',marker='x',ms=10,mew = 2)
                    DQ = max(QxAll)-min(QxAll) ; r = 0.02
                    lims = [min(QxAll)-r*DQ,max(max(QxAll),Qx0)+r*DQ,min(QyAll)-r*DQ,max(max(QyAll),Qy0)+r*DQ]
                    ax1.axis(lims)
                    plt.colorbar(fraction=0.046, pad=0.04)
                else: 
                    ax1 = fig.add_subplot(251,aspect = 'equal')
                    if nPart > 1e5:
                        ax1.scatter(xAll[::7],yAll[::7])
                    else:
                        ax1.scatter(xAll,yAll)
                    ax1.set_title('Distribution %d, Turn %1.e'%(Dist,iTurn))
                    ax1.set_xlabel(r'$x$') ; ax1.set_ylabel(r'$y$')

                # Plot horizontal phase space
                ax2 = fig.add_subplot(252,aspect = 'equal')
                ax2.set_title(r'Histogram of $(x,x\prime)$')
                ax2.set_xlabel(r'$x$') ; ax2.set_ylabel(r'$x\prime$')
                plt.hist2d(xAll,xpAll,weights = weight,bins = edges,cmap = maps[0],norm = LogNorm(),vmin=vmin)
                c1 =plt.colorbar(fraction=0.045, pad=0.05)
                ax2.grid(True)
                
                # Plot vertical phase space
                ax3 = fig.add_subplot(253,aspect = 'equal')
                ax3.set_title(r'Histogram of $(y,y\prime)$')
                ax3.set_xlabel(r'$y$') ; ax3.set_ylabel(r'$y\prime$')
                plt.hist2d(yAll,ypAll,weights = weight,bins = edges,cmap = maps[0],norm = LogNorm(),vmin = vmin)
                plt.colorbar(fraction=0.046, pad=0.04)
                ax3.grid(True)

                # Plot Longitudinal phase space
                ax3b = fig.add_subplot(254,aspect = 'equal')
                ax3b.set_title(r'Histogram of $(s,\delta)$')
                ax3b.set_xlabel(r'$s$') ; ax3b.set_ylabel(r'$\delta$')
                plt.hist2d(sAll,dAll,weights = weight,bins = edges,cmap = maps[0],norm = LogNorm(),vmin = vmin)
                plt.colorbar(fraction=0.046, pad=0.04)
                ax3b.grid(True)
                

                # Plot action space
                ax4 = fig.add_subplot(255,aspect = 'equal')
                ax4.set_title(r'Histogram of $(J_x,J_y)$')
                ax4.set_xlabel(r'$J_x$') ; ax4.set_ylabel(r'$J_y$')
                plt.hist2d(jxAll,jyAll,weights=weight,bins = nbins,cmap = maps[0],norm = LogNorm(),vmin = vmin)#,vmax = intensity)
                plt.colorbar(fraction=0.046, pad=0.04)
                ax4.grid(True)

                # Plot distribution in x                
                ax5 = fig.add_subplot(4,5,11)
                ax5.set_title(r'Histogram of $x$')
                val5,t,t = ax5.hist(xAll,bins = edges1D,weights=weight/dx)
                ax5.plot(x,gauss)
                ax9 = fig.add_subplot(4,5,16,sharex = ax5)
                ax9.plot(center1D,(val5-initX)/gaussPeak,'r.')
                ax9.set_title('Change, relative to peak')
                ax9.set_xlabel(r'$x$')            
                ax9.set_ylim([min(ax9.get_ylim()[0],-ticksize),max(ax9.get_ylim()[1],ticksize)])

                # Plot distribution in xp                
                ax6 = fig.add_subplot(4,5,12)
                ax6.set_title(r'Histogram of $x\prime$')
                val6,t,t = ax6.hist(xpAll,bins = edges1D,weights=weight/dx)
                ax6.plot(x,gauss)
                ax10 = fig.add_subplot(4,5,17,sharex = ax6)
                ax10.plot(center1D,(val6-initXP)/gaussPeak,'r.')
                ax10.set_title('Change, relative to peak')
                ax10.set_xlabel(r'$x\prime$')
                ax10.set_ylim([min(ax10.get_ylim()[0],-ticksize),max(ax10.get_ylim()[1],ticksize)])

                # Plot distribution in y
                ax7 = fig.add_subplot(4,5,13)
                ax7.set_title(r'Histogram of $y$')
                val7,t,t = ax7.hist(yAll,bins = edges1D,weights=weight/dx)
                ax7.plot(x,gauss)
                ax11 = fig.add_subplot(4,5,18,sharex = ax7)
                ax11.plot(center1D,(val7-initY)/gaussPeak,'r.')
                ax11.set_title('Change, relative to peak')
                ax11.set_xlabel(r'$y$')
                ax11.set_ylim([min(ax11.get_ylim()[0],-ticksize),max(ax11.get_ylim()[1],ticksize)])

                # Plot distribution in yp                                    
                ax8 = fig.add_subplot(4,5,14)
                ax8.set_title(r'Histogram of $y\prime$')
                val8,t,t = ax8.hist(ypAll,bins = edges1D,weights=weight/dx)
                ax8.plot(x,gauss)
                ax12 = fig.add_subplot(4,5,19,sharex = ax6)
                ax12.plot(center1D,(val8-initYP)/gaussPeak,'r.')
                ax12.set_title('Change, relative to peak')
                ax12.set_xlabel(r'$y\prime$')
                ax12.set_ylim([min(ax12.get_ylim()[0],-ticksize),max(ax12.get_ylim()[1],ticksize)])
#                ax12.yaxis.set_major_locator(ticker.MultipleLocator(ticksize))                                       

                # Title of the plot
                plt.suptitle(sys.argv[1]+'\nDistribution %d, Turn %1.e'%(Dist,iTurn),y=0.98)
                plt.tight_layout()
                plt.subplots_adjust(top=0.9)
                
                # Allow outpiut again
                if bool_stopstdout:
                    noOutput(False,save,null_fds )
                   
                ####################### 
                ### Save the figure ###
                if bool_savePlotPkl:
                    plt.savefig(plotdir+'/dist_'+sys.argv[1]+'_iT%1.e_%d'%(iTurn,iSnap))

                
                
    #############################################################
    ###################### PLOT EMITTANCES ######################
    if plot_depsdt and os.path.isfile(savedir +'/beamEvol.p') and nQ > 10:    
#        emittX = emittX6
#        emittY = emittY6
        # Possible function to fit evolution to
        def func(x,a,b):
            return a+b*x**2
        
        # Only fit beam emittances after tlim1 to a regression curve
        tlim1 = 5e5 if np.max(turns)>5e5 else np.max(turns)/2 
        if np.size(sys.argv)>4:
            tlim1 = float(eval(sys.argv[4]))

        # Stop output of warnings
        if bool_stopstdout:           
            save,null_fds = noOutput(True) 
        
        # Make emittance plotDistbeam   
        # ax[0] : Horizontal and vertical, all data
        # ax[1] : Horizontal, only data after first rematching has occured (J goes to approx stable value) + regression
        # ax[2] : Vertical, only data after first rematching has occured (J goes to approx stable value)   + regression 
        if np.sum(turns>tlim1)>1.5:
            fig,ax = plt.subplots(4,1,sharex = True,figsize=mods.plotsize(0,5))
            for i in range(4):
                # Plot horizontal
                if i <2: 
                    ax[i].plot(turns[turns>=i],emittX[turns>=i],'bo-',label = r'$\epsilon_x/N$')
                    if i>0:
                        dxdt,a = np.polyfit(turns[turns>=tlim1],emittX[turns>=tlim1],1)
                        ax[i].plot(turns[turns>=tlim1],a+dxdt*turns[turns>=tlim1],'c-',lw=3,label = '%.1e'%(dxdt))
                # Plot vertical 
                if i ==0 or i==2:
                    ax[i].plot(turns[turns>=i],emittY[turns>=i],'go-',label = r'$\epsilon_y/N$')
                    if i>0:
                        dydt,a = np.polyfit(turns[turns>=tlim1],emittY[turns>=tlim1],1)
                        ax[i].plot(turns[turns>=tlim1],a+dydt*turns[turns>=tlim1],'#00FF55',lw =3,label = '%.1e'%(dydt))
                if i==0 or i==3:
                    ax[i].plot(turns[turns>=i],emittZ[turns>=i],'ro-',label = r'$\epsilon_z/N$')
                    if i>0:
                        dzdt,a = np.polyfit(turns[turns>=tlim1],emittZ[turns>=tlim1],1)
                        ax[i].plot(turns[turns>=tlim1],a+dzdt*turns[turns>=tlim1],'orange',lw =3,label = '%.1e'%(dzdt))
                    
                        
                lgd = ax[i].legend(loc='center left', bbox_to_anchor=(1.0, 0.5),fancybox = True,fontsize=16)
                ax[i].margins(0.05)
                ax[i].set_ylabel('Beam emitt $\epsilon$')  

            # Fix titles and labels
            ax[2].set_xlabel('turn #') 
            ax[0].set_title('\n\nEmittance development over turns')
            plt.suptitle(sys.argv[1],fontsize = 15)
            plt.tight_layout()
            fig.subplots_adjust(right=0.8)

            # Save figure
            if bool_savePlotPkl:
                plt.savefig(plotdir+'/deps_'+sys.argv[1]+'.png')
        if bool_stopstdout:
            noOutput(False,save,null_fds )
                
        # Only for thesis plots
        if 1:
            indt = int(nQ/2)
            inds1 = (tlim1 <= turns)*(turns < turns[indt])
            inds2 = (turns[indt]<= turns)

            figt = plt.figure(figsize = mods.plotsize(-1,-2))
            plt.plot(turns,emittX,'bo-',label = r'$\epsilon_x$')
            if True: #1 regression
                dxdt,x0 = np.polyfit(turns[turns>=tlim1],emittX[turns>=tlim1],1)
                plt.plot(turns[turns>=tlim1],x0+dxdt*turns[turns>=tlim1],'c-',lw=3) #,label = '%.1e'%(dxdt))
            else:   #2 regressions
                dxdt,x0 = np.polyfit(turns[inds1],emittX[inds1],1)
                plt.plot(turns[inds1],x0+dxdt*turns[inds1],'c-',lw=3) #,label = '%.1e'%(dxdt))
                dxdt,x0 = np.polyfit(turns[inds2],emittX[inds2],1)
                plt.plot(turns[inds2],x0+dxdt*turns[inds2],'c-',lw=3) #,label = '%.1e'%(dxdt))

            plt.plot(turns,emittY,'go-',label = r'$\epsilon_y$')
            dydt,y0 = np.polyfit(turns[turns>=tlim1],emittY[turns>=tlim1],1)
            plt.plot(turns[turns>=tlim1],y0+dydt*turns[turns>=tlim1],'#00FF55',lw =3)#,label = '%.1e'%(dydt))
            
            yshift = (np.max(emittX)-np.min(emittX))*0.1
            plt.annotate(r'$\dot{\epsilon}_x = $%.2e'%dxdt, (turns[indt],emittX[indt]-yshift),fontsize=20)
            plt.annotate(r'$\dot{\epsilon}_y = $%.2e'%dydt, (turns[indt],emittY[indt]-yshift),fontsize=20)
            plt.margins(0.05)
            plt.ylabel(r'$\epsilon$')
            plt.xlabel(r'$T_\mathrm{sim}$')
            plt.legend(loc = 'best')
            plt.tight_layout()

            if bool_savePlotPkl:
                plt.savefig(plotdir+'/deps_1_'+sys.argv[1]+".png")
            
            stderx = np.std(emittX[turns>=tlim1]-x0-dxdt*turns[turns>=tlim1])
            stdery = np.std(emittY[turns>=tlim1]-y0-dydt*turns[turns>=tlim1])
            print('STDstot',stderx,stdery)
            print('STDsrel',np.std(emittX[turns>=tlim1]),np.std(emittY[turns>=tlim1]))
            print('initial change',(emittX[1]-emittX[0])/emittX[0], (emittY[1]-emittY[0])/emittY[0])
        
        # Plot other time evolutions
        if False:
            print('turn',turns)
            print('inte',beamIntensity)
            print('lumi',beamLuminosity)
        fig2,axes2 = plt.subplots(4,1,sharex = True,figsize=mods.plotsize(0,4))
        axes2[0].plot(turns[turns>0], beamIntensity[turns>0])
        axes2[0].set_title('\nBeam intensity')
        axes2[0].set_ylabel(r'$N$')
        axes2[1].plot(turns[turns>0], machIntensity[turns>0])
        axes2[1].set_title('\nMachine intensity')
        axes2[1].set_ylabel(r'$N$')

        axes2[2].plot(turns[turns>0], beamLuminosity[turns>0])
        axes2[2].set_title('Luminosity')
        axes2[2].set_ylabel(r'$L/L_0$')
        axes2[3].plot(turns[turns>0],beamSize[turns>0])
        axes2[3].set_title('Beamsize')
        axes2[3].set_ylabel(r'Area $[\sigma^2]$')
        plt.suptitle(sys.argv[1],fontsize = 15,)
        plt.tight_layout()
        fig2.subplots_adjust(top=0.8)
        
        t1 = 5e5 if np.max(turns)>5e5 else np.max(turns)/2 
        fLHC = const.c/26.7e3
        for i,arr in enumerate([beamIntensity,machIntensity]):
            fit = np.polyfit((turns[turns>t1])/(3600*fLHC),arr[turns>t1]/intensity,deg=1)
            lossRate_LHC = -fit[0]/fit[1]
            axes2[i].plot(turns[turns>t1],intensity*(fit[0]*turns[turns>t1]/(3600*fLHC)+fit[1]),
                        '--',lw=2,label=r'Lossrate$_{LHC} =$ %.1e /h'%lossRate_LHC)
            axes2[i].legend(loc='best',fontsize=14)
        
        
        if bool_savePlotPkl:
            plt.savefig(plotdir+'/dbeam_'+sys.argv[1]+'.png')
        
    ##########################################################################
    ############################## Plot Tunemap ##############################
    if plot_singleplots:
        print('Single plots')
        
        # Plot frequency diffusion maps
        if os.path.isfile(savedir+'/D_fma.p') : #and not bool_prime :
            # Import diffusion coefficients
            with open(savedir+'/D_fma.p','rb') as pklfile:
                D_fma = pkl.load(pklfile, encoding='latin1') 
            
            iSnap = 1
            while os.path.isfile(savedir + '/snap_iS%d.p'%iSnap) and  iSnap>0: 
             # Load snapfile
                with open(savedir + '/snap_iS%d.p'%iSnap,'rb') as pklfile:
                    abc = pkl.load(pklfile)  #, encoding='latin1'
                    [iTurn,xAll,xpAll,yAll,ypAll,sAll,dAll,weight,inBeamAll,inMachAll,iQ,
                        QxAll,QyAll,jxAll,jyAll,smearxAll,smearyAll,jzAll] = abc
                    inBeamAll = inBeamAll.astype(np.bool)
                    inMachAll = inMachAll.astype(np.bool)
                
                # Plot amplitude maps
                if iQ==1 and int(Dist/100)==3:
                    _ = mods.ampDiff(sys.argv[1],np.sqrt(2*jxAll),np.sqrt(2*jyAll)
                                    ,D_fma,20+iSnap*2+1,111,bool_save=1,app="_iQ%d"%iQ)            
                if iSnap == 1 and not int(Dist/100)==3:
                    _ = mods.ampDiff(sys.argv[1],np.sqrt(xAll**2+xpAll**2),np.sqrt(yAll**2+ypAll**2),
                                    D_fma,20+iSnap*2+1,111,bool_save = True,app="_iQ%d"%iQ)
#                    _ = mods.ampDiff(sys.argv[1],np.sqrt(2*jxAll),np.sqrt(2*jyAll),D_fma,20+iSnap*2+1,111,bool_save = True)            

                # Plot frequency map
                if iQ >= 1 and iQ<2: 
#                    ind = D_fma < np.log10(Qs/(2*midTurn))
#                    ind = D_fma<10
                    
#                    ind = np.logical_and(np.abs(QxAll - (Qx0-XI/2)) <1.5*XI, np.abs(QyAll - (Qy0-XI/2)) <1.5*XI)
#                    ind = np.logical_and(np.isfinite(QxAll+QyAll) , D_fma < -6)
                    ind = np.isfinite(QxAll+QyAll+D_fma)
                    ind[D_fma>-6] = False
                    try: 
                        print(np.max(D_fma), np.average(D_fma),np.log10(Qs/(2*midTurn)))
                    except: None
                    axs1 = mods.freqmapDiff(sys.argv[1],QxAll[ind],QyAll[ind],D_fma[ind],20+iSnap*2,111,bool_save=True,app="_iQ%d"%iQ)
                print("iQ = %d, iTurn = %1.e"%(iQ,iTurn))
                if iQ == 1 and Dist >=300 and Dist < 1000:
                    vmin = 0.1
                    if int(Dist/100)==3 or int(Dist/100)==5 :
                        vmin = 10**int(np.log10(np.min(weight)))
                        
                    fig = plt.figure(40+iSnap,figsize = mods.plotsize())
                    plt.hist2d(sAll,dAll,bins = 20)
                    fig = plt.figure(figsize=mods.plotsize(0,0))
                    ax1 = fig.add_subplot(111,aspect = 'equal')
                    ax1.set_title(r'Histogram of $(Q_x,Q_y)$'+"\n")
                    ax1.set_xlabel(r'$Q_x$') ; ax1.set_ylabel(r'$Q_y$')
                                        
                    if False:
                        [low, high] = [0,2]
                        asAll = np.sqrt(sAll**2+dAll**2)
                        print(np.min(asAll), np.max(asAll))
                        mask = np.all([low < asAll, asAll< high], axis=0)
                        add = "_%d-%d"%(low,high)
                    else:
                        add = ""
                        mask = np.abs(xAll)>-1
                    mask = mask*np.isfinite(QxAll+QyAll)
                    plt.hist2d(QxAll[mask],QyAll[mask],weights = weight[mask],bins = 10*(1+np.log(nPart)),cmap=maps[1],norm=LogNorm(),vmin=vmin,vmax = 1e9)
                    ax1.plot(Qx0,Qy0,c='r',marker='x',ms=10,mew = 2)

                    DQ = max(QxAll)-min(QxAll) ; r = 0.02
                    lims = [min(QxAll)-r*DQ,max(max(QxAll),Qx0)+r*DQ,min(QyAll)-r*DQ,max(max(QyAll),Qy0)+r*DQ]

                    ax1.axis(lims)
                    plt.colorbar(fraction=0.046, pad=0.04)
                    ax1.set_title(r'Histogram of $(Q_x,Q_y)$'+"\n")
                    fig.tight_layout()

                    sup1 = mods.maintitle(fig,sys.argv[1])
                    if bool_savePlotPkl:
                        plt.savefig(plotdir+'/fma_fh_'+sys.argv[1]+'_iT%1.e_%d'%(iTurn,iSnap) +add+'.png', 
                            bbox_inches='tight',bbox_extra_artists=[sup1])
                
                iSnap += 1                
    
    
    
    #######################################################################
    #################### DELETE FILES IF FEW PARTICLES ####################
    if (nPart<1e4 or nQ<0.5) and Dist > 99:
        os.system('rm -rf '+savedir)
        

    return showplots
    
if __name__ == '__main__':
    start = time.time()
    showplots = main()
    print(sys.argv[1])
    print(r'plotDistbeam.py: %d min, %.3f sek'%(np.floor((time.time()-start)/60),np.mod((time.time()-start),60)))
    sys.stdout.flush()
    if showplots:
        input()
