# SVF 
# Start 10.20.2016  - copied some from tuneDistbeam
# Do postprocessing other than calculating tunes

import numpy as np
import time
import os, sys
import pickle as pkl
import datetime as dt
if os.path.exists('11_Distbeam'):
    sys.path.append('11_Distbeam')
import moduleSondre as mods
from moduleSondre import noOutput

import matplotlib.pyplot as plt
#############################################################
#############################################################
def main():
    # Check that the given directory exists
    savedir = '02_pickle_save/'+sys.argv[1]
    if not os.path.exists(savedir):
        print('Given directory does not exists \n ' + savedir)
        sys.exit()
        
    if os.path.exists(savedir+"/post_done.txt"):
        print("Already done postprocessing")
        sys.exit()
    # Import parameters
    with open(savedir+'/parameters.p','rb') as pklfile:
        parameters = pkl.load(pklfile)   #, encoding='latin1'
        [IPS,Qx0,Qy0,Qx1,Qy1,XI,intensity,gamma,crossAS,betaIP, 
            emitNXW,emitNYW,emitNXS,emitNYS,Qs,Qs1, NR_G, lost_beam,lost_mach,
            headonC, nLR, longrangeC, normsepLR, noiseA,sigSW,sigDW,Qp,Qp1, maxA,nPart,inTurn,midTurn,ln2turn,
            nSave,nMean,nQ,Dist,nGroup,indGroup,turnArr,bool_savePlotPkl,bool_tuneArr,bool_tune]= parameters
   

    iSnap = 1
    # Preallocate memory to save results in 
    allQxAll = np.zeros((nQ,nPart))
    allQyAll = np.zeros((nQ,nPart))
    tuneTurns=np.zeros(nQ)
    D_fma = np.zeros(nPart)             #: Diffusion coefficients based on tune-wandering
    
    nSnaps = np.size(turnArr)-1         #: # of snapshots
    turns = np.zeros(nSnaps)            #: The number of turns taken before a given snapshot
    emittX = np.zeros(nSnaps)           #: Hori beamemittance of the entire beam
    emittY = np.zeros(nSnaps)           #: Vert beamemittance of the entire beam
    emittZ = np.zeros(nSnaps)           #: long beam"emittance" of the entire beam
    emittX6 = np.zeros(nSnaps)          #: Hori beamemittance of beam within 6σ
    emittY6 = np.zeros(nSnaps)          #: Vert beamemittance of beam within 6σ
    beamSize= np.zeros(nSnaps)          #: Real space size of the beam / normalized to σ^2 
    machSize= np.zeros(nSnaps)          #: Real space size of the beam / normalized to σ^2
    beamIntensity = np.zeros(nSnaps)    #: Beam intensity = number of particles still in the beam
    machIntensity = np.zeros(nSnaps)    #: Beam intensity = number of particles still in the machine 
    beamLuminosity =  np.zeros(nSnaps)      #: Luminosity relative to beginning (not IC, but first average)
    machLuminosity =  np.zeros(nSnaps)      #: Luminosity relative to beginning (not IC, but first average)
    ###############################
    ####### Post process ##########    
    while(os.path.isfile(savedir + '/snap_iS%d.p'%iSnap)):
        # Load snapfile
        with open(savedir + '/snap_iS%d.p'%iSnap,'rb') as pklfile:
            abc = pkl.load(pklfile)  #, encoding='latin1'
            [iTurn,xAll,xpAll,yAll,ypAll,sAll,dAll,weight,inBeamAll,inMachAll,iQ,
                        QxAll,QyAll,jxAll,jyAll,smearxAll,smearyAll,jzAll] = abc
            inBeamAll = inBeamAll.astype(np.bool)
            inMachAll = inMachAll.astype(np.bool)
            
#        print(np.average(jxAll,weights = weight), np.average(jyAll,weights=weight))
#        print(np.average(jxAll), np.average(jyAll))
        
        # Save important values          
        if bool_tune and nQ>1 and iQ>0:
            allQxAll[iQ-1,:] = QxAll
            allQyAll[iQ-1,:] = QyAll
            tuneTurns[iQ-1]=iTurn
        turns[iSnap-1] =  iTurn
        emittX[iSnap-1] = 2*np.average(jxAll,weights = weight)
        emittY[iSnap-1] = 2*np.average(jyAll,weights = weight) 
        emittZ[iSnap-1] = 2*np.average(jzAll,weights = weight) 
        emittX6[iSnap-1] = 2*np.average(jxAll[inMachAll],weights = weight[inMachAll]) if np.any(inMachAll) else 0
        emittY6[iSnap-1] = 2*np.average(jyAll[inMachAll],weights = weight[inMachAll]) if np.any(inMachAll) else 0
        
        beamIntensity[iSnap-1] = np.sum(weight[inBeamAll])              
        if np.sum(inBeamAll*1)>1.5:
            xav = np.average(xAll[inBeamAll],weights = weight[inBeamAll])   
            yav = np.average(yAll[inBeamAll],weights = weight[inBeamAll])   
            beamSize[iSnap-1] = (np.sqrt(np.average((xAll[inBeamAll]-xav)**2,weights=weight[inBeamAll]))*
                                np.sqrt(np.average((yAll[inBeamAll]-yav)**2,weights=weight[inBeamAll]))) 
            beamLuminosity[iSnap-1] = (beamIntensity[iSnap-1]/intensity)**2/(beamSize[iSnap-1])
        else:
            beamSize[iSnap-1] = 6
            beamLuminosity[iSnap-1] = 0

        machIntensity[iSnap-1] = np.sum(weight[inMachAll])            
        if np.sum(inMachAll*1) > 1.5: 
            xav = np.average(xAll[inMachAll],weights = weight[inMachAll])
            yav = np.average(yAll[inMachAll],weights = weight[inMachAll])
            machSize[iSnap-1] = (np.sqrt(np.average((xAll[inMachAll]-xav)**2,weights=weight[inMachAll]))*
                                np.sqrt(np.average((yAll[inMachAll]-yav)**2,weights=weight[inMachAll])))
            machLuminosity[iSnap-1] = (machIntensity[iSnap-1]/intensity)**2/(machSize[iSnap-1])
        else:
            machSize[iSnap-1] = 6
            machLuminosity[iSnap-1] = 0
        iSnap+=1
    
    # Normalize luminosity to initial value 
    turn1 = np.nonzero(turns)
    if not np.any(turn1): turn1 = -1
    beamLuminosity = beamLuminosity/beamLuminosity[turn1][0]
    machLuminosity = machLuminosity/machLuminosity[turn1][0]
    
    ##############################
    # Find diffusion coefficient #
    if bool_tune:
        
        ###################
        def smartPolyfit(x,Y):
            ind1 = np.isfinite(np.sum(Y,axis=0))
            ind2 = np.argsort(ind1)
            val = np.zeros_like(ind1,dtype=np.float64)    

            val[ind1] = np.polynomial.polynomial.polyfit(x,Y[:,ind1],deg=1)[1]
            for i in range(np.sum(~ind1)):
                w= np.isfinite(Y[:,ind2[i]])
                if np.sum(w)>1:
                    val[ind2[i]] = np.polynomial.polynomial.polyfit(x[w],Y[w,ind2[i]],deg=1)[1]
                else:
                    val[ind2[i]] = np.NaN
            return val
        
        # Not LHC specific - find on per turn basis - use 1 in calculations
        nTurnsPerHour = 11000*3600

        dx = smartPolyfit(tuneTurns/1,allQxAll)
        dy = smartPolyfit(tuneTurns/1,allQyAll)
        D_fma = np.log10(np.sqrt(dx**2+dy**2))
        
        # Save diffusion coefficients        
        with open(savedir + '/D_fma.p','wb') as pklfile:
            pkl.dump(D_fma,pklfile)
        
        
    ############################
    # Export time evolutions of different properties
    if bool_savePlotPkl:
        # Emittance evolution
        with open(savedir +'/beamEvol.p','wb') as pklfile:
            pkl.dump([turns, emittX,emittY, emittX6, emittY6,beamIntensity,beamSize,beamLuminosity,
                            machIntensity,machSize,machLuminosity, emittZ],pklfile)

        # Calculate depsdt.txt        
        tlim = 5e5 if np.max(turns)>5e5 else np.max(turns)/2
        inds = (turns>=tlim)
        if np.sum(inds)>2:
            
            
            coefx,covx = np.polyfit(turns[inds],emittX[inds],1,cov=True)
            coefy,covy = np.polyfit(turns[inds],emittY[inds],1,cov=True)
            dxdt = coefx[0] ; x0 = coefx[1] ; errx = np.sqrt(np.abs(covx[0,0]))
            dydt = coefy[0] ; y0 = coefy[1] ; erry = np.sqrt(np.abs(covy[0,0]))
            # Save deps.txt
            np.savetxt(savedir+'/deps.txt',[Qx0,Qy0,dxdt,dydt,x0,y0,errx,erry])


    #################################
    # Produce footprint-file
    if ln2turn>0:  # If tunes are calculated
        iSnap = 1 if int(Dist/100)!=3 else min(2,nSnaps)  # Want IC if not gaussian distribution
        with open(savedir + '/snap_iS%d.p'%iSnap,'rb') as pklfile:
            [iTurn,xAll,xpAll,yAll,ypAll,sAll,dAll,weight,inBeamAll,inMachAll,iQ,
                        QxAll,QyAll,jxAll,jyAll,smearxAll,smearyAll,jzAll]= pkl.load(pklfile)
            RX = np.sqrt(2*jxAll)
            RY = np.sqrt(2*jyAll)
        QxAll = allQxAll[0,:]
        QyAll = allQyAll[0,:]
        
        with open(savedir + "/footprint.p",'wb') as pklfile:
            pkl.dump([RX,RY,QxAll,QyAll,D_fma],pklfile)
            
            
    ###############################
    # Delete unnecessary iSnaps
    if nSnaps > 10 and ln2turn < 0:
        for i in range(1,nSnaps+1):
            if i>2 and (i%max(int(nSnaps/5),1) >0 or nSnaps > 49) and i < nQ+1 and not(i== int(nQ/2)):
                os.remove(savedir+ '/snap_iS%d.p'%(i))
            
    # Make a file to look for to check if post processing is done
    np.savetxt(savedir+"/post_done.txt",[1])
        
#############################################################
#############################################################
#############################################################
if __name__ == '__main__':
    start = time.time()
    print('\npostDistbeam.py    - '+str(dt.datetime.now().time())[:8])
    print(sys.argv[1])
    main()
    print(r'postDistbeam: %d min, %.3f sek'%(np.floor((time.time()-start)/60),np.mod((time.time()-start),60)))

#    np.savetxt('filename.txt',[sys.argv[1]],delimiter = " ",fmt = "%s")
