#SVF 
#Start 09.09.2016
#06.02.2017: Only able to use Jacobsen - not Pysussix
import os, sys
import numpy as np
import time
import datetime as dt
import matplotlib
import matplotlib.pyplot as plt
import scipy.constants as const
import scipy.special as spec

#from PySussix import Sussix

import pickle as pkl
import multiprocessing as mp

if os.path.exists('11_Distbeam'):
    sys.path.append('11_Distbeam')
import moduleSondre as mods
from moduleSondre import noOutput
from jacobsen import *

nProcs = min(15,mp.cpu_count()-1)   #: # of parallel processes at a time
#############################################################
#############################################################
def calculateTunesFromfile():
    print('\ntuneDistbeam.py    - '+str(dt.datetime.now().time())[:8])
    print(sys.argv[1])

    # Check that the given directory exists
    savedir = '02_pickle_save/'+sys.argv[1]
    if not os.path.exists(savedir):
        print('Given directory does not exists (tuneDistbeam.py) \n ' + savedir)
        sys.exit(1)

    #Running parameters
    bool_rmSussfile = True              #: if want to delete sussfiles after saving to snapfile
    bool_stopstdout = True              #: If want to stop output from Sussix
    tune_method = 1                     #: 1:  assisted Jacobsen ( Pysussix is option 2)
    start = time.time()



    # Import parameters
    with open(savedir+'/parameters.p','rb') as pklfile:
        parameters = pkl.load(pklfile, encoding='latin1')  
        [IPS,Qx0,Qy0,Qx1,Qy1,XI,intensity,gamma,crossAS,betaIP, 
            emitNXW,emitNYW,emitNXS,emitNYS,Qs,Qs1, NR_G, lost_beam,lost_mach,
            headonC, nLR, longrangeC, normsepLR, noiseA,sigSW,sigDW,Qp,Qp1, maxA,nPart,inTurn,midTurn,ln2turn,
            nSave,nMean,nQ,Dist,nGroup,indGroup,turnArr,bool_savePlotPkl,bool_tuneArr,bool_tune] = parameters
#    if Qp1+Qp2 > 5 and (maxA%2==0 or Dist > 611): tune_method = 2


    # Prepare to collect results    
    def collect_result(res):
        results.append(res) 
        
    ###################################################
    ####### Calculate tune for each snapshot ##########
    iSnap = 1
    while (os.path.isfile(savedir + '/snap_iS%d.p'%iSnap)) and bool_tune:
    
        # Load snapfile
        with open(savedir + '/snap_iS%d.p'%iSnap,'rb') as pklfile:
            abc = pkl.load(pklfile)   #, encoding='latin1'
            
            [iTurn,xAll,xpAll,yAll,ypAll,sAll,dAll,weight,inBeamAll,inMachAll,iQ,
                        QxAll,QyAll,jxAll,jyAll,smearxAll,smearyAll,jzAll]  = abc

        # Only calculate tune if not already done
        if iQ>0  and np.size(QxAll)==1:   
            if np.size(QxAll) == 1:
                QxAll = np.zeros_like(xAll)
                QyAll = np.zeros_like(xAll)
            ## PYSUSSIX    
##            if tune_method == 2 :
##                Sussix().sussix_inp(nt1 = 1, nt2 =nSave,idam=2,ir=1,tunex=Qx0,tuney=Qy0)  #Write sussix input file

            ## Paralellisation
            results = []
            pool = mp.Pool(processes = nProcs)
            for group in range(nGroup):              
                args = [iQ,group,start,bool_stopstdout,tune_method,savedir,indGroup
                    ,nSave,nQ,nGroup,XI,Qx0,Qy0,Qp,Qs,0,0]
                p = pool.apply_async(groupSuss,args = (args,),callback = collect_result)
            pool.close()
            pool.join()
            ## Collect results
            for i in range(nGroup):
                QxSome,QySome,i1,i2 = results[i]
                QxAll[i1:i2] = QxSome
                QyAll[i1:i2] = QySome

            #################
            # Clean up files (i.e. delete large files containing multiple turns of a group of particles)
            with open(savedir+'/snap_iS%d.p'%(iSnap),'wb') as pklfile:
                pkl.dump([iTurn,xAll,xpAll,yAll,ypAll,sAll,dAll,weight,inBeamAll,inMachAll,
                iQ,QxAll,QyAll,jxAll,jyAll,smearxAll,smearyAll,jzAll],pklfile,1)
            group = 0
            while bool_rmSussfile and os.path.isfile(savedir+ '/multsnap_iQ%d_iG%d.p'%(iQ,group)):
                os.remove(savedir+ '/multsnap_iQ%d_iG%d.p'%(iQ,group))
                group +=1
        iSnap += 1                

#############################################################
#############################################################
def calculateTunesDirectly(i1,i2,nSave,XI,Qx0,Qy0,Qp,Qs,xMat,yMat,QxAll,QyAll):

    indLoc = np.linspace(0,i2-i1,nProcs+1).astype(np.int32)

    def collect_result(res):
        results.append(res) 
        
    #Paralellisation
    tune_method = 1

    results = []
    pool = mp.Pool(processes = nProcs)

    for ind in range(indLoc.size-1):  
        j1 = indLoc[ind]  ; j2 = indLoc[ind+1]            
        argsTune = [0,ind,0,1,tune_method,"",indLoc,nSave,0,0,XI,Qx0,Qy0,Qp,Qs,
        xMat[j1:j2,:],yMat[j1:j2,:]]
        p = pool.apply_async(groupSuss,args = (argsTune,),callback = collect_result)

    pool.close()
    pool.join()
    # Collect results
    for ind in range(indLoc.size-1):
        QxSome,QySome,j1,j2 = results[ind]
        j1 = j1+i1 ; j2 = j2+i1
        QxAll[j1:j2] = QxSome
        QyAll[j1:j2] = QySome
    return indLoc

#############################################################
#############################################################
def findpeak(tempX,cox,Qx0,Qs):
    ratlim = 20
    cnsp = 0.1
    
    ind = np.all((np.int32(Qx0*2)<2*tempX, np.int32(2*tempX)<2*Qx0, np.abs(tempX-Qx0)<0.2 ,cox>np.max(cox)/ratlim),axis=0)
    tempX =tempX[ind]  ;  cox = cox[ind]
    ind = np.argsort(tempX)
    tempX = tempX[ind] ;  cox = cox[ind]     
    c = (np.cumsum(cox)-1*cox/2)/np.sum(cox)
    QQ = tempX[np.argmin(np.abs(c-0.5))]           
    if np.size(c)>1:
        if np.min(np.abs(c[1:]+c[:-1]-1))<cnsp and (abs(np.abs(np.diff(tempX[np.argsort(np.abs(c-0.5))][:2]))-Qs*2)<Qs/10
                                                    and QQ != tempX[np.argmax(cox)]) :
            QQ = np.NaN #np.average(tempX[np.argsort(np.abs(c-0.5))][:2])
    return QQ
    
def groupSuss(args):
    [iQ,group,start,bool_stopstdout,tune_method,savedir,indGroup
        ,nSave,nQ,nGroup,XI,Qx0,Qy0,Qp,Qs,xMat,yMat] = args
    i1,i2 = indGroup[group:group+2]
    
#    XI = float('0.'+savedir[savedir.find('_X')+2:savedir.find('_A')])
    # Load sussfile if not given in arguments
    if np.size(xMat) == 1:
        if not os.path.isfile(savedir+ '/multsnap_iQ%d_iG%d.p'%(iQ,group)):
            print('File does not exist: ' + savedir+ '/suss_iQ%d_iG%d.p'%(iQ,group))
            sys.exit(1)
        with open(savedir+ '/multsnap_iQ%d_iG%d.p'%(iQ,group),'rb') as pklfile:
            [xMat,yMat] = pkl.load(pklfile)
        bool_direct=False
    else:
        bool_direct=True
    #############################################
    # ######### Tune analysis ###################
    # ###########################################
    QxSome = np.zeros(i2-i1)
    QySome = np.zeros(i2-i1)
    
    nFix = [0,0]
    # Jacobsen
    if tune_method == 1: 
        bool_Qp_prob = Qp>0 and False
        num_harmonics = 5 if not bool_Qp_prob else 20
        for p in range(i2-i1):
            tempX,cox = np.abs(np.array(laskar_method_time_space(xMat[p,:],num_harmonics)))
            tempY,coy = np.abs(np.array(laskar_method_time_space(yMat[p,:],num_harmonics)))
            if bool_Qp_prob:
                QxSome[p] = findpeak(tempX,cox,Qx0,Qs)
                QySome[p] = findpeak(tempY,coy,Qy0,Qs)
                
            else:
                indx = np.argsort(np.concatenate((-cox,-cox)))
                indy = np.argsort(np.concatenate((-coy,-coy)))

                tempX = np.concatenate((tempX,1-tempX))[indx] ;  tempY = np.concatenate((tempY,1-tempY))[indy]
                try:
                    QxSome[p] = next(tempX[i] for i,v in enumerate(tempX) if ( int(Qx0*2)<2*v and int(2*v)<2*Qx0 and abs(v-(Qx0-abs(XI)))<(abs(XI)*3 )))
                except:
                    print('OHOH x')
                    QxSome[p] = None
                try:
                    QySome[p] = next(tempY[i] for i,v in enumerate(tempY) if ( int(Qy0*2)<2*v and int(2*v)<2*Qy0 and abs(v-(Qy0-abs(XI)))<(abs(XI)*3 )))
                except:
                    print('NONO y')
                    QySome[p] = None
                
##                
##    elif tune_method ==2:
##        # PySussix
##        z = np.zeros(nSave)
##        sussix = Sussix()

##        if bool_stopstdout:           
##            save,null_fds = noOutput(True)          #: Homemade function to repress output from PySussix until ↓
##        for p in range(i2-i1) :
##            sussix.sussix(xMat[p,:],z,yMat[p,:],z,z,z)
##            QxSome[p] = sussix.ox[0]
##            QySome[p] = sussix.oy[0]
##        if bool_stopstdout:
##            noOutput(False,save,null_fds )          #: Homemade function to accept output again after ↑

    else:
        print('Tune method does not exist')
        sys.exit()

    if np.sum(nFix) > 0:
        print("Tunes fixed: ",[np.isnan(QxSome).sum,np.isnan(QySome).sum])
    ################
    # Output to user
    if group+1 == nGroup and not bool_direct:
        print('%d OF %d TUNES, %d OF %d GROUPS, %d TURNS, %d min, %.3f sek'%(iQ, nQ, group+1, nGroup, \
        nSave,np.floor((time.time()-start)/60),np.mod((time.time()-start),60)))
    
    res = (QxSome, QySome, i1,i2) 
    return res


#############################################################
#############################################################
def main():
    calculateTunesFromfile()

#############################################################
#############################################################
if __name__ == '__main__':
    start = time.time()
    main()
    print(r'tuneDistbeam: %d min, %.3f sek'%(np.floor((time.time()-start)/60),np.mod((time.time()-start),60)))
    
    
    


