README

Information is not up to date

12-20-2016
This is a first draft of a weak-strong simulation code for HO interaction

launchDistbeam.py:      launches the code, takes same input as GPUtrackDistbeam.py and CPUtrackDistbeam.pycuda
CPUtrackDistbeam.py:    Does tracking using multiple CPUs (if a GPU is not available) can not do chromaticity
GPUtrackDistbeam.py:    Does tracking using GPU
tuneDistbeam.py:        Does tune calculations. As of now, GPUtrackDistbeam calls this function to avoid saving lots of files. CPUtrackDistbeam does not
    jacobsen.py:        Code from Jaime, instead of PySussix, for calculating tunes
postDistbeam.py:        Postprocessing. Calculates emittance, emittance for particles withing 6σ, luminosity etc.
plotDistbeam.py:        Plots initial results of tracking. Plots snapshots with histograms in real space, phase space and tune space
moduleSondre.py:        Some different functions for making initial condition and plotting.


Calling the program. Two main options:
1) Call launchDistbeam.py, this will again call tracking (GPU), tuneDistbeam, postDistbeam, plotDistbeam
2) Call CPUtrackDistbeam or GPUtrackDistbeam and then call tuneDistbeam, postDistbeam, plotDistbeam

OBS: As of now, call the files from outside the folder, and do not change the name of the folder (sorry). i.e.
python 11_Distbeam/launchDistbeam.py...


How to call the program (same for launchDistbeam, GPUtrackDistbeam, CPUtrackDistbeam)
* You have to send in 13 arguments (in addition to the filename)
python3 11_Distbeam/launchDistbeam.py  IPS Qx Qy XI maxA N iT mT p nQ Dist_Y_P dQ Qp
#   name    example     explanation
1   IPS     101010      Says what kind of tracking is to be done. 1=yes. 0=no. 
                        HvHhNC. H=headon, v=LR vertical, h=LR horizontal (LR only in CPU), N=noise,C=chromaticity (only in GPU)
2   Qx0     0.31        Horizontal total tune
3   Qy0     0.32        Vertical total tune
4   XI      0.015       Beam-beam parameter (of each of two IPs)
5   maxA    6           Max amplitude of distribution. Only relevant for distributions that are not gaussian (see later)
6   N       1e5         Number of macro-particles
7   iT      1           number of THOUSANDS of turns before the first tune measurement (if you want to calculate tunes)
8   mT      10          number of THOUSANDS of turns between snapshots/tune measurements 
9   p       1           Precision for tune measurements. turndata = 1024*2**p. if p<0, tunes are not calculated (and code is faster)
10  nQ      10          Number of tunes/snapshots after IC snapshot. 
11  Dist_Y_P 311        What initial condition to use. DYP. 
                            D = which distribution. 1=uniform in 1st quadrant, 2=2D uniform gaussian weight, 3=Gaussian, 4=up to 4D uniform gaussian
                            Y = condition on y-plane. 0: y-amplitude is 0, 1= y distributed as x, 2=y is set equal to y
                            P = condition on momentums. 0: xp,yp-IC amplitude =0, 1: distributed as x
12  dQ      0.125       Tune for first part of the beamline. 0.125 will try to cancel 4th order resonance. usually use 0
13  Qp      2           Chromaticity value. usually (2,15). Value only matters if last integer in IPS is not 0



Example: 
1) For tune diffusion:
    python3 11_Distbeam/launchDistbeam.py 101000 0.31 0.32 0.015 10 1e4 0 10 1 4 110 0 0
2) For beam emittance growth:
    python3 11_Distbeam/launchDistbeam.py 101010 0.31 0.32 0.015 0 1e5 0 20 -1 100 311 0 0


Other parts also have special ways of calling them.  
